package com.getmydrivercard.androidapp.enums;

public enum ApplicationType {
    NEW, RENEWAL, REPLACEMENT, EXCHANGE
}
