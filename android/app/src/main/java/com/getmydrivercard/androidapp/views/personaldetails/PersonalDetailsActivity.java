package com.getmydrivercard.androidapp.views.personaldetails;

import android.content.Intent;
import android.os.Bundle;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.enums.PreviousActivity;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.personaldetails2.PersonalDetails2Activity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class PersonalDetailsActivity extends BaseDrawerActivity implements PersonalDetailsContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    PersonalDetailsFragment mView;

    @Inject
    PersonalDetailsContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);
        ButterKnife.bind(this);
        mView.setPresenter(mPresenter);
        mView.setNavigator(this);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPersonalDetailsNew(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto) {
            Intent intent = new Intent(this, PersonalDetails2Activity.class);
            intent.putExtra("applicationType", (ApplicationType)getIntent().getExtras().get("applicationType"));
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            intent.putExtra("PreviousActivity", PreviousActivity.NEW);
            intent.putExtra("application", applyingForFirstDigitalTachographCardDto);
            startActivity(intent);
            finish();
    }

    @Override
    public void navigateToPersonalDetailsExchange(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto) {
        Intent intent = new Intent(this, PersonalDetails2Activity.class);
        intent.putExtra("applicationType", ApplicationType.EXCHANGE);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("PreviousActivity", PreviousActivity.EXCHANGE);
        intent.putExtra("application", exchangingEUCardForBGCardDto);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsReplacement(ReplacementOfCardDto replacementOfCardDto) {
        Intent intent = new Intent(this, PersonalDetails2Activity.class);
        intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("PreviousActivity", PreviousActivity.REPLACEMENT1);
        intent.putExtra("application", replacementOfCardDto);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsRenewal(RenewalOfCardDto renewalOfCardDto) {
        Intent intent = new Intent(this, PersonalDetails2Activity.class);
        intent.putExtra("applicationType", ApplicationType.RENEWAL);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("PreviousActivity", PreviousActivity.RENEWAL);
        intent.putExtra("application", renewalOfCardDto);
        startActivity(intent);
        finish();
    }

}
