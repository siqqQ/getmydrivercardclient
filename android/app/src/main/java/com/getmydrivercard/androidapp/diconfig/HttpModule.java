package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.Constants;
import com.getmydrivercard.androidapp.http.HttpRequester;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class HttpModule {
    @Provides
    public HttpRequester httpRequester() {
        return new OkHttpHttpRequester();
    }

    @Provides
    @Named("baseServerUrl")
    public String baseServerUrl() {
        return Constants.BASE_SERVER_URL;
    }
}
