package com.getmydrivercard.androidapp.views.myapplications;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.PreviousActivity;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.menu.MenuContracts;
import com.getmydrivercard.androidapp.views.menu.MenuFragment;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class MyApplicationsActivity extends BaseDrawerActivity implements MyApplicationsContracts.Navigator {
    public static final long IDENTIFIER = 300;

    @Inject
    MyApplicationsFragment mView;

    @Inject
    MyApplicationsContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_applications);
        ButterKnife.bind(this);

        mView.setPresenter(mPresenter);
        mView.setNavigator(this);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPersonalDetailsNew() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.NEW);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsExchange() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.EXCHANGE);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsRenewal() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.RENEWAL);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsReplacement1() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.REPLACEMENT1);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsReplacement2() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.REPLACEMENT2);
        startActivity(intent);
        finish();
    }
}