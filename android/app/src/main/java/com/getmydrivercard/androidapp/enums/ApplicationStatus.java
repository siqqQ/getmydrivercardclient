package com.getmydrivercard.androidapp.enums;

public enum ApplicationStatus {
    PENDING, REJECTED, APPROVED
}
