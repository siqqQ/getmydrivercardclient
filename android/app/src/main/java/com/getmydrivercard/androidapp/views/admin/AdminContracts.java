package com.getmydrivercard.androidapp.views.admin;

public interface AdminContracts {
    interface View {

        void setPresenter(AdminContracts.Presenter presenter);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(AdminContracts.View view);

        void unsubscribe();
    }

    interface Navigator {
    }
}
