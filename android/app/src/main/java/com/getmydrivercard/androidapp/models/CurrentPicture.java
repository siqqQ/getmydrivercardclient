package com.getmydrivercard.androidapp.models;

public enum CurrentPicture {
    SELFIE_PHOTO, ID_CARD_PHOTO, DRIVING_LICENSE_PHOTO, SIGNATURE_FOR_DECLARATION_PHOTO, PREVIOUS_TACHOGRAPH_CARD_PHOTO
}
