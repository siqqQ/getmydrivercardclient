package com.getmydrivercard.androidapp.views.myapplications;

public interface MyApplicationsContracts {
    interface View {

        void setPresenter(MyApplicationsContracts.Presenter presenter);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(MyApplicationsContracts.View view);

        void unsubscribe();
    }

    interface Navigator {
        void navigateToPersonalDetailsNew();
        void navigateToPersonalDetailsExchange();
        void navigateToPersonalDetailsRenewal();
        void navigateToPersonalDetailsReplacement1();
        void navigateToPersonalDetailsReplacement2();
    }
}
