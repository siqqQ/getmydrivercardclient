package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.login.LoginContracts;
import com.getmydrivercard.androidapp.views.login.LoginFragment;
import com.getmydrivercard.androidapp.views.login.LoginPresenter;
import com.getmydrivercard.androidapp.views.menu.MenuContracts;
import com.getmydrivercard.androidapp.views.menu.MenuFragment;
import com.getmydrivercard.androidapp.views.menu.MenuPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LoginModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract LoginFragment loginFragment();

    @ActivityScoped
    @Binds
    abstract LoginContracts.Presenter loginPresenter(LoginPresenter presenter);
}
