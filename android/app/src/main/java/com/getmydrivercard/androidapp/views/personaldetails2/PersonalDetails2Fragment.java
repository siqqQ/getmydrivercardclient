package com.getmydrivercard.androidapp.views.personaldetails2;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.models.AddressDto;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.EUCountry;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.PhoneNumberDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.validators.Patterns;

import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalDetails2Fragment extends Fragment implements PersonalDetails2Contracts.View {


    private PersonalDetails2Contracts.Presenter mPresenter;

    private PersonalDetails2Contracts.Navigator mNavigator;

    @BindView(R.id.spinner_personalDetails2_countryAddress)
    Spinner countryAddress;

    @BindView(R.id.editText_personalDetails2_area)
    EditText area;

    @BindView(R.id.editText_personalDetails2_city)
    EditText city;

    @BindView(R.id.editText_personalDetails2_street)
    EditText street;

    @BindView(R.id.spinner_personalDetails2_countryNumber)
    Spinner countryNumber;

    @BindView(R.id.editText_personalDetails2_number)
    EditText number;

    @BindView(R.id.button_personalDetails2_savePersonalDetails)
    Button savePersonalDetails;

    @Inject
    public PersonalDetails2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_details2, container, false);
        area = view.findViewById(R.id.editText_personalDetails2_area);
        area.setText("Sofia");
        area.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    area.setHint("");
                else
                    area.setHint("Write You Area Here");
            }
        });
        city = view.findViewById(R.id.editText_personalDetails2_city);
        city.setText("Sofia");
        city.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    city.setHint("");
                else
                    city.setHint("Write You City Here");
            }
        });
        street = view.findViewById(R.id.editText_personalDetails2_street);
        street.setText("Sttown");
        street.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    street.setHint("");
                else
                    street.setHint("Write You Street Here");
            }
        });
        number = view.findViewById(R.id.editText_personalDetails2_number);
        number.setText("4324432");
        number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    number.setHint("");
                else
                    number.setHint("Write You Number Here");
            }
        });
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.button_personalDetails2_savePersonalDetails)
    public void savePersonalDetails(){
        if (!Pattern.matches(Patterns.PATTERN_STRING_PLUS_SPACE, area.getText().toString())){
            area.setError("Area Is Not Valid");
        }
        else if (!Pattern.matches(Patterns.PATTERN_STRING_PLUS_SPACE, city.getText().toString())){
            city.setError("City Is Not Valid");
        }
        else if (!Pattern.matches(Patterns.PATTERN_STRING_PLUS_SPACE, street.getText().toString())){
            street.setError("Street Is Not Valid");
        }
        else if (!Pattern.matches(Patterns.PATTERN_NUMBER, number.getText().toString())){
            street.setError("Number Is Not Valid");
        }
        else{
            if (getActivity().getIntent().getExtras().get("applicationType") == ApplicationType.NEW){
                ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto = (ApplyingForFirstDigitalTachographCardDto) getActivity().getIntent().getExtras().getSerializable("application");
                applyingForFirstDigitalTachographCardDto.getPersonalDetailsDto().setAddressDto(new AddressDto(EUCountry.valueOf(countryAddress.getSelectedItem().toString().toUpperCase()), area.getText().toString(), city.getText().toString(), street.getText().toString()));
                applyingForFirstDigitalTachographCardDto.getPersonalDetailsDto().setPhoneNumberDto(new PhoneNumberDto(EUCountry.valueOf(countryNumber.getSelectedItem().toString().toUpperCase()),number.getText().toString()));
                mNavigator.navigateToNew(applyingForFirstDigitalTachographCardDto);
            }
            else if (getActivity().getIntent().getExtras().get("applicationType") == ApplicationType.EXCHANGE){
                ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto) getActivity().getIntent().getExtras().getSerializable("application");
                exchangingEUCardForBGCardDto.getPersonalDetailsDto().setAddressDto(new AddressDto(EUCountry.valueOf(countryAddress.getSelectedItem().toString().toUpperCase()), area.getText().toString(), city.getText().toString(), street.getText().toString()));
                exchangingEUCardForBGCardDto.getPersonalDetailsDto().setPhoneNumberDto(new PhoneNumberDto(EUCountry.valueOf(countryNumber.getSelectedItem().toString().toUpperCase()),number.getText().toString()));
                mNavigator.navigateToExchange(exchangingEUCardForBGCardDto);
            }
            else if (getActivity().getIntent().getExtras().get("applicationType") == ApplicationType.REPLACEMENT){
                ReplacementOfCardDto replacementOfCardDto = (ReplacementOfCardDto) getActivity().getIntent().getExtras().getSerializable("application");
                replacementOfCardDto.getPersonalDetailsDto().setAddressDto(new AddressDto(EUCountry.valueOf(countryAddress.getSelectedItem().toString().toUpperCase()), area.getText().toString(), city.getText().toString(), street.getText().toString()));
                replacementOfCardDto.getPersonalDetailsDto().setPhoneNumberDto(new PhoneNumberDto(EUCountry.valueOf(countryNumber.getSelectedItem().toString().toUpperCase()),number.getText().toString()));
                mNavigator.navigateToReplacement(replacementOfCardDto);
            }
            else if (getActivity().getIntent().getExtras().get("applicationType") == ApplicationType.RENEWAL){
                RenewalOfCardDto renewalOfCardDto = (RenewalOfCardDto) getActivity().getIntent().getExtras().getSerializable("application");
                renewalOfCardDto.getPersonalDetails().setAddressDto(new AddressDto(EUCountry.valueOf(countryAddress.getSelectedItem().toString().toUpperCase()), area.getText().toString(), city.getText().toString(), street.getText().toString()));
                renewalOfCardDto.getPersonalDetails().setPhoneNumberDto(new PhoneNumberDto(EUCountry.valueOf(countryNumber.getSelectedItem().toString().toUpperCase()),number.getText().toString()));
                mNavigator.navigateToRenewal(renewalOfCardDto);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(PersonalDetails2Contracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void navigateToPersonalDetails(PersonalDetailsDto personalDetailsDto) {

    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(PersonalDetails2Contracts.Navigator navigator) {
        mNavigator = navigator;
    }

}
