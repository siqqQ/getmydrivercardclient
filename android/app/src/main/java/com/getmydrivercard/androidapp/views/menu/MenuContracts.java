package com.getmydrivercard.androidapp.views.menu;

public interface MenuContracts {
    interface View {

        void setPresenter(MenuContracts.Presenter presenter);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(MenuContracts.View view);

        void unsubscribe();
    }

    interface Navigator {
        void navigateToPersonalDetailsNew();
        void navigateToPersonalDetailsExchange();
        void navigateToPersonalDetailsRenewal();
        void navigateToPersonalDetailsReplacement1();
        void navigateToPersonalDetailsReplacement2();
    }
}
