package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardContracts;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardFragment;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class NewTahographCardModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract NewTahographCardFragment newTahographCardFragment();

    @ActivityScoped
    @Binds
    abstract NewTahographCardContracts.Presenter pewTahographCardPresenter(NewTahographCardPresenter presenter);
}
