package com.getmydrivercard.androidapp.views.register;

public interface RegisterContracts {
    interface View {

        void setPresenter(RegisterContracts.Presenter presenter);

        void navigateToPersonalDetails(String response3);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(RegisterContracts.View view);

        void unsubscribe();
    }

    public interface Navigator {

        void navigateToPersonalDetails(String response3);
    }
}
