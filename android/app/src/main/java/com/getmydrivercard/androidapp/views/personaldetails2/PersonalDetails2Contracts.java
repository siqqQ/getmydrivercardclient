package com.getmydrivercard.androidapp.views.personaldetails2;

import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;

public interface PersonalDetails2Contracts {
    interface View {

        void setPresenter(PersonalDetails2Contracts.Presenter presenter);

        void navigateToPersonalDetails(PersonalDetailsDto personalDetailsDto);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(PersonalDetails2Contracts.View view);

        void unsubscribe();
    }

    interface Navigator {

        void navigateToNew(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto);
        void navigateToExchange(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto);
        void navigateToReplacement(ReplacementOfCardDto replacementOfCardDto);
        void navigateToRenewal(RenewalOfCardDto renewalOfCardDto);
    }
}
