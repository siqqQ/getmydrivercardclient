package com.getmydrivercard.androidapp.views.newtahographcard;

import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.NewTachographCardSpecial;

public interface NewTahographCardContracts {
    interface View {

        void setPresenter(NewTahographCardContracts.Presenter presenter);

        void navigateToPersonalDetails(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(NewTahographCardContracts.View view);

        void unsubscribe();
    }

    interface Navigator {

        void navigateToPersonalDetails(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto);
    }
}
