package com.getmydrivercard.androidapp.views.welcome;

public interface WelcomeContracts {
    interface View {

        void setPresenter(WelcomeContracts.Presenter presenter);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(WelcomeContracts.View view);

        void unsubscribe();
    }

    public interface Navigator {
        void navigateToLoginPage();
        void navigateToRegisterPage();
    }
}
