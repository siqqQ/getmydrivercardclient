package com.getmydrivercard.androidapp.views.replacement;

import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;

public interface ReplacementContracts {
    interface View {

        void setPresenter(ReplacementContracts.Presenter presenter);

        void navigateToPersonalDetails(ReplacementOfCardDto replacementOfCardDto);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(ReplacementContracts.View view);

        void unsubscribe();
    }

    interface Navigator {

        void navigateToPersonalDetails(ReplacementOfCardDto replacementOfCardDto);
    }
}