package com.getmydrivercard.androidapp.views.applicationdetails;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.PreviousActivity;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.admin.AdminActivity;
import com.getmydrivercard.androidapp.views.menu.MenuActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class ApplicationDetailsActivity extends BaseDrawerActivity implements ApplicationDetailsContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    ApplicationDetailsFragment mView;

    @Inject
    ApplicationDetailsContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_details);
        ButterKnife.bind(this);

        mView.setPresenter(mPresenter);
        mView.setNavigator(this);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToAdmin() {
        Intent intent = new Intent(getBaseContext(), AdminActivity.class);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToMenu() {
        Intent intent = new Intent(getBaseContext(), MenuActivity.class);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        startActivity(intent);
        finish();
    }
}
