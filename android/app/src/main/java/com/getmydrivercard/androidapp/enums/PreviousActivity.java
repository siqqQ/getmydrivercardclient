package com.getmydrivercard.androidapp.enums;

public enum PreviousActivity {
    NEW, REPLACEMENT1, REPLACEMENT2, EXCHANGE, RENEWAL, REGISTER
}
