package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.register.RegisterContracts;
import com.getmydrivercard.androidapp.views.register.RegisterFragment;
import com.getmydrivercard.androidapp.views.register.RegisterPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class RegisterModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract RegisterFragment registerFragment();

    @ActivityScoped
    @Binds
    abstract RegisterContracts.Presenter registerPresenter(RegisterPresenter presenter);
}
