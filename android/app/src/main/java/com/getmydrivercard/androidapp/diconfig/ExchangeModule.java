package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.exchange.ExchangeContracts;
import com.getmydrivercard.androidapp.views.exchange.ExchangeFragment;
import com.getmydrivercard.androidapp.views.exchange.ExchangePresenter;
import com.getmydrivercard.androidapp.views.login.LoginContracts;
import com.getmydrivercard.androidapp.views.login.LoginFragment;
import com.getmydrivercard.androidapp.views.login.LoginPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class ExchangeModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract ExchangeFragment exchangeFragment();

    @ActivityScoped
    @Binds
    abstract ExchangeContracts.Presenter exchangePresenter(ExchangePresenter presenter);
}
