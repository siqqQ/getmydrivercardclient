package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsContracts;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsFragment;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsPresenter;
import com.getmydrivercard.androidapp.views.pictures.PicturesContracts;
import com.getmydrivercard.androidapp.views.pictures.PicturesFragment;
import com.getmydrivercard.androidapp.views.pictures.PicturesPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PicturesModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract PicturesFragment picturesFragment();

    @ActivityScoped
    @Binds
    abstract PicturesContracts.Presenter picturesPresenter(PicturesPresenter presenter);
}

