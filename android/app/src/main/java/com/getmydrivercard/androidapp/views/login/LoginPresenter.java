package com.getmydrivercard.androidapp.views.login;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;
import com.getmydrivercard.androidapp.views.menu.MenuContracts;

import javax.inject.Inject;

public class LoginPresenter implements LoginContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private LoginContracts.View mView;

    @Inject
    public LoginPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(LoginContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }

}
