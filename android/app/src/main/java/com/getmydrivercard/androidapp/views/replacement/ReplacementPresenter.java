package com.getmydrivercard.androidapp.views.replacement;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardContracts;

import javax.inject.Inject;

public class ReplacementPresenter implements ReplacementContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private ReplacementContracts.View mView;

    @Inject
    public ReplacementPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(ReplacementContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }
}
