package com.getmydrivercard.androidapp.views.pictures;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;
import com.getmydrivercard.androidapp.models.AddressDto;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.DrivingLicenseCategory;
import com.getmydrivercard.androidapp.models.DrivingLicenseDto;
import com.getmydrivercard.androidapp.models.EUCountry;
import com.getmydrivercard.androidapp.models.NewTachographCardSpecial;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.PhoneNumberDto;
import com.getmydrivercard.androidapp.models.PhotoDto;
import com.getmydrivercard.androidapp.models.UserDto;
import com.getmydrivercard.androidapp.views.register.RegisterContracts;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class PicturesFragment extends Fragment implements PicturesContracts.View {

    private PicturesContracts.Presenter mPresenter;
//
//    @BindView(R.id.button_register_registerUser)
//    Button registerUserButton;

    private static int RESULT_LOAD_IMAGE = 1;
    private PicturesContracts.Navigator mNavigator;

    @Inject
    public PicturesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pictures, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(PicturesContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void navigateToPersonalDetails() {
        mNavigator.navigateToPersonalDetails();
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(PicturesContracts.Navigator navigator) {
        mNavigator = navigator;
    }

}
