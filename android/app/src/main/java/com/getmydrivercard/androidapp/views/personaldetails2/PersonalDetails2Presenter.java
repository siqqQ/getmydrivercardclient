package com.getmydrivercard.androidapp.views.personaldetails2;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsContracts;

import javax.inject.Inject;

public class PersonalDetails2Presenter implements PersonalDetails2Contracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private PersonalDetails2Contracts.View mView;

    @Inject
    public PersonalDetails2Presenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(PersonalDetails2Contracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }
}
