package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.welcome.WelcomeContracts;
import com.getmydrivercard.androidapp.views.welcome.WelcomeFragment;
import com.getmydrivercard.androidapp.views.welcome.WelcomePresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class WelcomeModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract WelcomeFragment welcomeFragment();

    @ActivityScoped
    @Binds
    abstract WelcomeContracts.Presenter welcomePresenter(WelcomePresenter presenter);
}
