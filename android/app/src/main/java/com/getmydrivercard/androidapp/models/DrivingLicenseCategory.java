package com.getmydrivercard.androidapp.models;

import java.io.Serializable;

public enum DrivingLicenseCategory implements Serializable {
    C1, C, D1, D, BE, C1E, CE, D1E, DE
}

