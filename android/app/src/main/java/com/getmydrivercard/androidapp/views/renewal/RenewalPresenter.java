package com.getmydrivercard.androidapp.views.renewal;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;

import javax.inject.Inject;

public class RenewalPresenter implements RenewalContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private RenewalContracts.View mView;

    @Inject
    public RenewalPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(RenewalContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }
}
