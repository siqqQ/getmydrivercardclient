package com.getmydrivercard.androidapp.views.register;

import android.content.Intent;
import android.os.Bundle;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.PreviousActivity;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.menu.MenuActivity;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class RegisterActivity extends BaseDrawerActivity implements RegisterContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    RegisterFragment mView;

    @Inject
    RegisterContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        mView.setPresenter(mPresenter);
        mView.setNavigator(this);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPersonalDetails(String response3) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.REGISTER);
        intent.putExtra("token", response3);
        startActivity(intent);
        finish();
    }
}
