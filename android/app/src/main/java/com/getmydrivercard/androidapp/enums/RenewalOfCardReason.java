package com.getmydrivercard.androidapp.enums;

public enum RenewalOfCardReason {
    DUE_TO_EXPIRE, SUSPENDED, WITHDRAWN
}
