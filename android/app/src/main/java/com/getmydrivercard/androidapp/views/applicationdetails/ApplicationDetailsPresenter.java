package com.getmydrivercard.androidapp.views.applicationdetails;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;
import com.getmydrivercard.androidapp.views.login.LoginContracts;

import javax.inject.Inject;

public class ApplicationDetailsPresenter implements ApplicationDetailsContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private ApplicationDetailsContracts.View mView;

    @Inject
    public ApplicationDetailsPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(ApplicationDetailsContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }

}
