package com.getmydrivercard.androidapp.views.pictures;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.enums.PreviousActivity;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;
import com.getmydrivercard.androidapp.models.ApplicationsDetailsDto;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.CurrentPicture;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.PhotoDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.applicationdetails.ApplicationDetailsActivity;
import com.getmydrivercard.androidapp.views.menu.MenuActivity;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PicturesActivity extends BaseDrawerActivity implements PicturesContracts.Navigator {

    // Camera activity request code
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static String imageStoragePath;

    private static int RESULT_LOAD_IMAGE = 1;
    public static final long IDENTIFIER = 290;

    @Inject
    PicturesFragment mView;

    @Inject
    PicturesContracts.Presenter mPresenter;

    @BindView(R.id.button_pictures_save)
    Button save;

    @BindView(R.id.frameLayout_pictures_cameraPreview)
    FrameLayout cameraPreview;

    @BindView(R.id.imageView_pictures_imageView)
    ImageView imageView;

    @BindView(R.id.txt_desc)
    TextView nextPicture;

    @BindView(R.id.button_pictures_capture)
    Button capture;

    @BindView(R.id.btnCamera)
    Button takeAPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures);
        ButterKnife.bind(this);

        mView.setPresenter(mPresenter);
        mView.setNavigator(this);

        if (getIntent().getExtras().containsKey("image")){
            Log.d("i am here", "signature");
            imageView = (ImageView) findViewById(R.id.imageView_pictures_imageView);
            Log.d("bitmapa", getIntent().getExtras().get("image").toString());
            Bitmap bitmap2 = StringToBitMap(getIntent().getExtras().get("image").toString());
            imageView.setImageBitmap(bitmap2);
            imageView.setBackgroundColor(Color.BLACK);
        }
        if (getIntent().getExtras().containsKey("signature")){
            imageView = (ImageView) findViewById(R.id.imageView_pictures_imageView);
            String byteArray = getIntent().getExtras().getString("signature");
            Log.d("signatureSign", byteArray);
            Bitmap bitmap = StringToBitMap(byteArray);
            nextPicture.setVisibility(View.INVISIBLE);
            imageView.setImageBitmap(bitmap);
        }
        CurrentPicture currentPicture = CurrentPicture.valueOf(getIntent().getExtras().get("currentPicture").toString().toUpperCase());
        if (currentPicture == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO){
            //save.setVisibility(View.INVISIBLE);
            capture.setVisibility(View.INVISIBLE);
            takeAPicture.setText("Sign");
        }
        Button buttonLoadImage = (Button) findViewById(R.id.button_pictures_capture);
        if (!getIntent().getExtras().containsKey("signature") && imageView.getDrawable() == null){
            nextPicture.setText("Picture Will Be Shown Here After You Choose One \n Current Picture To Make \n " + currentPicture.toString().replace("_", " ").toLowerCase());
        }
        else {
            nextPicture.setVisibility(View.INVISIBLE);
        }
        buttonLoadImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        if (!CameraUtils.isDeviceSupportCamera(getApplicationContext())) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device doesn't have camera
            finish();
        }

        Button btnCapturePicture = findViewById(R.id.btnCapturePicture);
        btnCapturePicture.setOnClickListener(v -> {
            if (CameraUtils.checkPermissions(getApplicationContext())) {
                captureImage();
            } else {
                requestCameraPermission();
            }
        });

        Button btnCamera = findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener((v) -> {
            Intent intent = new Intent(PicturesActivity.this, CameraActivity.class);
            if (getIntent().getExtras().get("applicationType") == ApplicationType.NEW) {
                ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto = (ApplyingForFirstDigitalTachographCardDto) getIntent().getExtras().getSerializable("application");
                intent.putExtra("application", applyingForFirstDigitalTachographCardDto);
                if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.NEW);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.NEW);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.NEW);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                    intent = new Intent(this, DrawActivity.class);
                    intent.putExtra("application", applyingForFirstDigitalTachographCardDto);
                    intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.NEW);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else {
                    intent = new Intent(this, MenuActivity.class);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                }
                startActivity(intent);
                finish();
            } else if (getIntent().getExtras().get("applicationType") == ApplicationType.EXCHANGE) {
                ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto) getIntent().getExtras().getSerializable("application");
                intent.putExtra("application", exchangingEUCardForBGCardDto);
                if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.EXCHANGE);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.EXCHANGE);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                    Log.d("photo", "driving license");
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.EXCHANGE);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                    Log.d("photo", "driving license");
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.EXCHANGE);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                    Log.d("photo", "driving license");
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                    intent = new Intent(this, DrawActivity.class);
                    intent.putExtra("application", exchangingEUCardForBGCardDto);
                    intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.EXCHANGE);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else {
                    intent = new Intent(this, MenuActivity.class);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                }
                startActivity(intent);
                finish();
            }
            else if (getIntent().getExtras().get("applicationType") == ApplicationType.REPLACEMENT) {
                ReplacementOfCardDto replacementOfCardDto = (ReplacementOfCardDto) getIntent().getExtras().getSerializable("application");
                intent.putExtra("application", replacementOfCardDto);
                if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                    Log.d("photo", "driving license");
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                    intent = new Intent(this, DrawActivity.class);
                    intent.putExtra("application", replacementOfCardDto);
                    intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else {
                    intent = new Intent(this, MenuActivity.class);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                }
                startActivity(intent);
                finish();
            }
            else if (getIntent().getExtras().get("applicationType") == ApplicationType.RENEWAL) {
                RenewalOfCardDto renewalOfCardDto = (RenewalOfCardDto) getIntent().getExtras().getSerializable("application");
                intent.putExtra("application", renewalOfCardDto);
                if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.RENEWAL);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.RENEWAL);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.RENEWAL);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                    intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.RENEWAL);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                    intent = new Intent(this, DrawActivity.class);
                    intent.putExtra("application", renewalOfCardDto);
                    intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
                    intent.putExtra("applicationType", ApplicationType.RENEWAL);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                } else {
                    intent = new Intent(this, MenuActivity.class);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                }
                startActivity(intent);
                finish();
            }
        });

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPersonalDetails() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.REGISTER);
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPicturesActivityNew(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.REGISTER);
        intent.putExtra("application", applyingForFirstDigitalTachographCardDto);
        if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
            intent.putExtra("applicationType", ApplicationType.NEW);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            Log.d("photo", "id card photo");
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
            intent.putExtra("applicationType", ApplicationType.NEW);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            Log.d("photo", "driving license");
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
            intent.putExtra("applicationType", ApplicationType.NEW);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else {
            intent = new Intent(this, ApplicationDetailsActivity.class);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            intent.putExtra("application", applyingForFirstDigitalTachographCardDto);
            ApplicationsDetailsDto applicationsDetailsDto = new ApplicationsDetailsDto(null, applyingForFirstDigitalTachographCardDto.getApplicationType(),
                    applyingForFirstDigitalTachographCardDto.getApplicationStatus(), applyingForFirstDigitalTachographCardDto.getPersonalDetailsDto(), applyingForFirstDigitalTachographCardDto.getSelfiePhoto(),
                    applyingForFirstDigitalTachographCardDto.getIdCardPhoto(), applyingForFirstDigitalTachographCardDto.getDrivingLicensePhoto(),applyingForFirstDigitalTachographCardDto.getSignatureForDeclarationPhoto(),
                    null, null, null, null, null, null,
                    null, null, null, null, null, applyingForFirstDigitalTachographCardDto.getDrivingLicenseDto());
            intent.putExtra("aplicationDetails", applicationsDetailsDto);
            intent.putExtra("applicationAfterMadeDetails", "true");
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPicturesActivityExchange(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.REGISTER);
        intent.putExtra("application", exchangingEUCardForBGCardDto);
        if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
            intent.putExtra("applicationType", ApplicationType.EXCHANGE);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
            intent.putExtra("applicationType", ApplicationType.EXCHANGE);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            Log.d("photo", "driving license");
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
            intent.putExtra("applicationType", ApplicationType.EXCHANGE);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
            intent.putExtra("applicationType", ApplicationType.EXCHANGE);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else {
            intent = new Intent(this, ApplicationDetailsActivity.class);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            intent.putExtra("application", exchangingEUCardForBGCardDto);
            ApplicationsDetailsDto applicationsDetailsDto = new ApplicationsDetailsDto(null, exchangingEUCardForBGCardDto.getApplicationType(),
                    exchangingEUCardForBGCardDto.getApplicationStatus(), exchangingEUCardForBGCardDto.getPersonalDetailsDto(), exchangingEUCardForBGCardDto.getSelfiePhoto(),
                    exchangingEUCardForBGCardDto.getIdCardPhoto(), exchangingEUCardForBGCardDto.getDrivingLicensePhoto(),
                    exchangingEUCardForBGCardDto.getSignatureForDeclarationPhoto(),
                    exchangingEUCardForBGCardDto.getPreviousTachographCardPhoto(), exchangingEUCardForBGCardDto.getEUCountryOfIssuing(), exchangingEUCardForBGCardDto.getTachographCardNumber(),
                    exchangingEUCardForBGCardDto.getCountryOfIssuingOfDrivingLicense(), exchangingEUCardForBGCardDto.getDrivingLicenseNumber(), null,
                    null, null, null, null, null, null);
            intent.putExtra("aplicationDetails", applicationsDetailsDto);
            intent.putExtra("applicationAfterMadeDetails", "true");
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPicturesActivityReplacement(ReplacementOfCardDto replacementOfCardDto) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.REGISTER);
        intent.putExtra("application", replacementOfCardDto);
        if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
            intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
            intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
            intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
            intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else {
            intent = new Intent(this, ApplicationDetailsActivity.class);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            intent.putExtra("application", replacementOfCardDto);
            ApplicationsDetailsDto applicationsDetailsDto = new ApplicationsDetailsDto(null, replacementOfCardDto.getApplicationType(),
                    replacementOfCardDto.getApplicationStatus(), replacementOfCardDto.getPersonalDetailsDto(), replacementOfCardDto.getSelfiePhoto(),
                    replacementOfCardDto.getIdCardPhoto(), replacementOfCardDto.getDrivingLicensePhoto(),
                    replacementOfCardDto.getSignatureForDeclarationPhoto(),
                    replacementOfCardDto.getPreviousTachographCardPhoto(), null, replacementOfCardDto.getTachographCardNumber(),
                    null, null, replacementOfCardDto.getReplacementOfCardReason(),
                    replacementOfCardDto.getIssuingAuthority(), replacementOfCardDto.getDateOfExpiry(), replacementOfCardDto.getLossOrTheftDate(),
                    null, null, null);
            intent.putExtra("aplicationDetails", applicationsDetailsDto);
            intent.putExtra("applicationAfterMadeDetails", "true");
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPicturesActivityRenewal(RenewalOfCardDto renewalOfCardDto) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("PreviousActivity", PreviousActivity.RENEWAL);
        intent.putExtra("application", renewalOfCardDto);
        if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
            intent.putExtra("applicationType", ApplicationType.RENEWAL);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
            intent.putExtra("applicationType", ApplicationType.RENEWAL);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
            intent.putExtra("applicationType", ApplicationType.RENEWAL);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
            intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
            intent.putExtra("applicationType", ApplicationType.RENEWAL);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
        } else {
            intent = new Intent(this, ApplicationDetailsActivity.class);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            intent.putExtra("application", renewalOfCardDto);
            ApplicationsDetailsDto applicationsDetailsDto = new ApplicationsDetailsDto(null, renewalOfCardDto.getApplicationType(),
                    renewalOfCardDto.getApplicationStatus(), renewalOfCardDto.getPersonalDetails(), renewalOfCardDto.getSelfiePhoto(),
                    renewalOfCardDto.getIdCardPhoto(), renewalOfCardDto.getDrivingLicensePhoto(),
                    renewalOfCardDto.getSignatureForDeclarationPhoto(),
                    renewalOfCardDto.getPreviousTachographCardPhoto(), renewalOfCardDto.getCountryOfIssuing(), renewalOfCardDto.getTachographCardNumber(),
                    null, null, null,
                    renewalOfCardDto.getIssuingAuthority(), null, null,
                    null, renewalOfCardDto.getRenewalOfCardReason(), null);
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToMenu() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //CAMERA:
        if (resultCode == RESULT_OK) {
            // Refreshing the gallery
            CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);

            // successfully captured the image
            // display it in image view
            previewCapturedImage();
        } else if (resultCode == RESULT_CANCELED) {
            // user cancelled Image capture
            Toast.makeText(getApplicationContext(),
                    "User cancelled image capture", Toast.LENGTH_SHORT)
                    .show();
        } else {
            // failed to capture image
            Toast.makeText(getApplicationContext(),
                    "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                    .show();
        }
        //END CAMERA
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.imageView_pictures_imageView);
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            imageView.setImageBitmap(bitmap);

        }
    }

    @OnClick(R.id.button_pictures_save)
    public void onSuperheroSaveClicked() {
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageInByte2 = baos.toByteArray();
        String imageInByte = new String(imageInByte2);
        String item = getIntent().getExtras().getString("token");
        if (getIntent().getExtras().get("applicationType") == ApplicationType.NEW) {
            ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto = null;
            if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                applyingForFirstDigitalTachographCardDto = (ApplyingForFirstDigitalTachographCardDto) getIntent().getExtras().getSerializable("application");
                applyingForFirstDigitalTachographCardDto.setSelfiePhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                applyingForFirstDigitalTachographCardDto = (ApplyingForFirstDigitalTachographCardDto) getIntent().getExtras().getSerializable("application");
                applyingForFirstDigitalTachographCardDto.setIdCardPhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                applyingForFirstDigitalTachographCardDto = (ApplyingForFirstDigitalTachographCardDto) getIntent().getExtras().getSerializable("application");
                applyingForFirstDigitalTachographCardDto.setDrivingLicensePhoto(new PhotoDto(BitMapToString(bitmap)));
                Log.d("SIGNATURE", imageInByte.toString());
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                applyingForFirstDigitalTachographCardDto = (ApplyingForFirstDigitalTachographCardDto) getIntent().getExtras().getSerializable("application");
                applyingForFirstDigitalTachographCardDto.setSignatureForDeclarationPhoto(new PhotoDto(BitMapToString(bitmap)));
                applyingForFirstDigitalTachographCardDto.setSignatureForDeclarationPhoto(new PhotoDto(BitMapToString(bitmap)));
            }
            navigateToPicturesActivityNew(applyingForFirstDigitalTachographCardDto);
        } else if (getIntent().getExtras().get("applicationType") == ApplicationType.EXCHANGE) {
            ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto = null;
            if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto) getIntent().getExtras().getSerializable("application");
                exchangingEUCardForBGCardDto.setSelfiePhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto) getIntent().getExtras().getSerializable("application");
                exchangingEUCardForBGCardDto.setIdCardPhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto) getIntent().getExtras().getSerializable("application");
                exchangingEUCardForBGCardDto.setDrivingLicensePhoto(new PhotoDto(BitMapToString(bitmap)));
                Log.d("SIGNATURE", imageInByte.toString());
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto) getIntent().getExtras().getSerializable("application");
                exchangingEUCardForBGCardDto.setPreviousTachographCardPhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto) getIntent().getExtras().getSerializable("application");
                exchangingEUCardForBGCardDto.setSignatureForDeclarationPhoto(new PhotoDto(BitMapToString(bitmap)));
            }
            navigateToPicturesActivityExchange(exchangingEUCardForBGCardDto);
        }
        else if (getIntent().getExtras().get("applicationType") == ApplicationType.REPLACEMENT) {
            ReplacementOfCardDto replacementOfCardDto = null;
            if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                replacementOfCardDto = (ReplacementOfCardDto) getIntent().getExtras().getSerializable("application");
                replacementOfCardDto.setSelfiePhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                replacementOfCardDto = (ReplacementOfCardDto) getIntent().getExtras().getSerializable("application");
                replacementOfCardDto.setIdCardPhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                replacementOfCardDto = (ReplacementOfCardDto) getIntent().getExtras().getSerializable("application");
                replacementOfCardDto.setDrivingLicensePhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                replacementOfCardDto = (ReplacementOfCardDto) getIntent().getExtras().getSerializable("application");
                replacementOfCardDto.setPreviousTachographCardPhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                replacementOfCardDto = (ReplacementOfCardDto) getIntent().getExtras().getSerializable("application");
                replacementOfCardDto.setSignatureForDeclarationPhoto(new PhotoDto(BitMapToString(bitmap)));
            }
            navigateToPicturesActivityReplacement(replacementOfCardDto);
        }
        else if (getIntent().getExtras().get("applicationType") == ApplicationType.RENEWAL) {
            RenewalOfCardDto renewalOfCardDto = null;
            if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                renewalOfCardDto = (RenewalOfCardDto) getIntent().getExtras().getSerializable("application");
                renewalOfCardDto.setSelfiePhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                renewalOfCardDto = (RenewalOfCardDto) getIntent().getExtras().getSerializable("application");
                renewalOfCardDto.setIdCardPhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                renewalOfCardDto = (RenewalOfCardDto) getIntent().getExtras().getSerializable("application");
                renewalOfCardDto.setDrivingLicensePhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                renewalOfCardDto = (RenewalOfCardDto) getIntent().getExtras().getSerializable("application");
                renewalOfCardDto.setPreviousTachographCardPhoto(new PhotoDto(BitMapToString(bitmap)));
            } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                renewalOfCardDto = (RenewalOfCardDto) getIntent().getExtras().getSerializable("application");
                renewalOfCardDto.setSignatureForDeclarationPhoto(new PhotoDto(BitMapToString(bitmap)));
            }
            navigateToPicturesActivityRenewal(renewalOfCardDto);
        }
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    //CAMERA:
    private void restoreFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(CameraUtils.KEY_IMAGE_STORAGE_PATH)) {
                imageStoragePath = savedInstanceState.getString(CameraUtils.KEY_IMAGE_STORAGE_PATH);
                if (!TextUtils.isEmpty(imageStoragePath)) {
                    if (imageStoragePath.substring(imageStoragePath.lastIndexOf(".")).equals("." + CameraUtils.IMAGE_EXTENSION)) {
                        previewCapturedImage();
                    }
                }
            }
        }
    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            captureImage();
                        } else if (report.isAnyPermissionPermanentlyDenied()) {
                            showPermissionsAlert();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    /**
     * Capturing Camera Image will launch camera app requested image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile();
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Saving stored image path to saved instance state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putString(CameraUtils.KEY_IMAGE_STORAGE_PATH, imageStoragePath);
    }

    /**
     * Restoring image path from saved instance state
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        imageStoragePath = savedInstanceState.getString(CameraUtils.KEY_IMAGE_STORAGE_PATH);
    }


    /**
     * Display image from gallery
     */
    private void previewCapturedImage() {
        try {
            TextView txtPreview = findViewById(R.id.txt_desc);
            txtPreview.setVisibility(View.GONE);

            ImageView imgPreview = findViewById(R.id.imgPreview);
            imgPreview.setVisibility(View.VISIBLE);
            Bitmap bitmap = CameraUtils.optimizeBitmap(CameraUtils.BITMAP_SAMPLE_SIZE, imageStoragePath);
            imgPreview.setImageBitmap(bitmap);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", (dialog, which) -> CameraUtils.openSettings(PicturesActivity.this))
                .setNegativeButton("CANCEL", (dialog, which) -> {
                }).show();
    }

}

