package com.getmydrivercard.androidapp.views.pictures;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.util.Base64;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.CurrentPicture;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.PhotoDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;

import java.io.ByteArrayOutputStream;


public class CameraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Camera deviceCamera = Camera.open();

        ImageSurfaceView imageSurfaceView = new ImageSurfaceView(CameraActivity.this, deviceCamera);
        FrameLayout cameraPreviewLayout = findViewById(R.id.camera_preview);
        cameraPreviewLayout.addView(imageSurfaceView);

        Button captureButton = findViewById(R.id.button_camera_proceed);
        captureButton.setOnClickListener(v -> takePicture(deviceCamera));
    }

    private void takePicture(Camera deviceCamera) {
        deviceCamera.takePicture(
                null,
                null,
                (data, camera) -> {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    if (bitmap == null) {
                        Toast.makeText(CameraActivity.this, "Captured image is empty", Toast.LENGTH_LONG).show();
                        return;
                    }


                    ImageView capturedImageHolder = findViewById(R.id.captured_image);


                    capturedImageHolder.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 300, 200, true));
                    Bitmap bitmap2 = ((BitmapDrawable) capturedImageHolder.getDrawable()).getBitmap();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] imageInByte = baos.toByteArray();
                    String uploadPicture = new String(imageInByte);
                    Intent intent = new Intent(this, PicturesActivity.class);
                    intent.putExtra("token", getIntent().getExtras().get("token").toString());
                    if (getIntent().getExtras().get("applicationType") == ApplicationType.NEW) {
                        intent.putExtra("applicationType", ApplicationType.NEW);
                        ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto = (ApplyingForFirstDigitalTachographCardDto) getIntent().getExtras().getSerializable("application");
                        if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
                            applyingForFirstDigitalTachographCardDto.setSignatureForDeclarationPhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                            applyingForFirstDigitalTachographCardDto.setDrivingLicensePhoto(new PhotoDto(uploadPicture));
                            intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                            applyingForFirstDigitalTachographCardDto.setIdCardPhoto(new PhotoDto(uploadPicture));
                            intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                            applyingForFirstDigitalTachographCardDto.setSelfiePhoto(new PhotoDto(uploadPicture));
                            intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
                        }
                        intent.putExtra("image", BitMapToString(bitmap2));
                        intent.putExtra("application", applyingForFirstDigitalTachographCardDto);
                        startActivity(intent);
                        finish();
                    } else if (getIntent().getExtras().get("applicationType") == ApplicationType.EXCHANGE) {
                        intent.putExtra("applicationType", ApplicationType.EXCHANGE);
                        ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto) getIntent().getExtras().getSerializable("application");
                        if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
                            exchangingEUCardForBGCardDto.setSignatureForDeclarationPhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
                            exchangingEUCardForBGCardDto.setDrivingLicensePhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
                            exchangingEUCardForBGCardDto.setIdCardPhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
                            exchangingEUCardForBGCardDto.setSelfiePhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
                            exchangingEUCardForBGCardDto.setPreviousTachographCardPhoto(new PhotoDto(uploadPicture));
                        }
                        intent.putExtra("image", BitMapToString(bitmap2));
                        intent.putExtra("application", exchangingEUCardForBGCardDto);
                        startActivity(intent);
                        finish();
                    }
                    else if (getIntent().getExtras().get("applicationType") == ApplicationType.REPLACEMENT) {
                        intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
                        ReplacementOfCardDto replacementOfCardDto = (ReplacementOfCardDto) getIntent().getExtras().getSerializable("application");
                        if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
                            replacementOfCardDto.setSignatureForDeclarationPhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
                            replacementOfCardDto.setDrivingLicensePhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
                            replacementOfCardDto.setIdCardPhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
                            replacementOfCardDto.setSelfiePhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
                            replacementOfCardDto.setPreviousTachographCardPhoto(new PhotoDto(uploadPicture));
                        }
                        intent.putExtra("image", BitMapToString(bitmap2));
                        intent.putExtra("application", replacementOfCardDto);
                        startActivity(intent);
                        finish();
                    }
                    else if (getIntent().getExtras().get("applicationType") == ApplicationType.RENEWAL) {
                        intent.putExtra("applicationType", ApplicationType.RENEWAL);
                        RenewalOfCardDto renewalOfCardDto = (RenewalOfCardDto) getIntent().getExtras().getSerializable("application");
                        if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
                            renewalOfCardDto.setSignatureForDeclarationPhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.DRIVING_LICENSE_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.DRIVING_LICENSE_PHOTO);
                            renewalOfCardDto.setDrivingLicensePhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.ID_CARD_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.ID_CARD_PHOTO);
                            renewalOfCardDto.setIdCardPhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.SELFIE_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
                            renewalOfCardDto.setSelfiePhoto(new PhotoDto(uploadPicture));
                        } else if (getIntent().getExtras().get("currentPicture") == CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO) {
                            intent.putExtra("currentPicture", CurrentPicture.PREVIOUS_TACHOGRAPH_CARD_PHOTO);
                            renewalOfCardDto.setPreviousTachographCardPhoto(new PhotoDto(uploadPicture));
                        }
                        intent.putExtra("image", BitMapToString(bitmap2));
                        intent.putExtra("application", renewalOfCardDto);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
}

