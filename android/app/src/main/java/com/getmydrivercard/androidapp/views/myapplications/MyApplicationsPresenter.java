package com.getmydrivercard.androidapp.views.myapplications;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;
import com.getmydrivercard.androidapp.views.menu.MenuContracts;

import javax.inject.Inject;

public class MyApplicationsPresenter implements MyApplicationsContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private MyApplicationsContracts.View mView;

    @Inject
    public MyApplicationsPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(MyApplicationsContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }

}
