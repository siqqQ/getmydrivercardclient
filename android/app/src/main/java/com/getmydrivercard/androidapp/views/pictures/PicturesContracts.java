package com.getmydrivercard.androidapp.views.pictures;

import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;

public interface PicturesContracts {
    interface View {

        void setPresenter(PicturesContracts.Presenter presenter);

        void navigateToPersonalDetails();

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(PicturesContracts.View view);

        void unsubscribe();
    }

    public interface Navigator {

        void navigateToPersonalDetails();
        void navigateToPicturesActivityNew(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto);
        void navigateToPicturesActivityExchange(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto);
        void navigateToPicturesActivityReplacement(ReplacementOfCardDto replacementOfCardDto);
        void navigateToPicturesActivityRenewal(RenewalOfCardDto renewalOfCardDto);
        void navigateToMenu();
    }
}
