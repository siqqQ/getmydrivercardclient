package com.getmydrivercard.androidapp.views.renewal;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.RenewalOfCardReason;
import com.getmydrivercard.androidapp.models.EUCountry;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;

import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RenewalFragment extends Fragment implements RenewalContracts.View {


    private RenewalContracts.Presenter mPresenter;

    private RenewalContracts.Navigator mNavigator;

    @BindView(R.id.editText_renewal_issuingAuthority)
    EditText issuingAuthority;

    @BindView(R.id.button_renewal_proceed)
    Button proceed;

    @BindView(R.id.editText_renewal_tachographCardNumber)
    EditText tachographCardNumber;

    @BindView(R.id.spinner_renewal_countryIssuing)
    Spinner countryIssuing;

    @BindView(R.id.spinner_renewal_reason)
    Spinner reason;

    @BindView(R.id.textView_renewal_dateOfExpirityCurrentCard)
    TextView dateOfExpirityCurrentCard;

    @Inject
    public RenewalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_renewal, container, false);
        ButterKnife.bind(this, view);
        issuingAuthority.setText("MVR");
        tachographCardNumber.setText("132221321");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(RenewalContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void navigateToPersonalDetails(RenewalOfCardDto renewalOfCardDto) {
        mNavigator.navigateToPersonalDetails(renewalOfCardDto);
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(RenewalContracts.Navigator navigator) {
        mNavigator = navigator;
    }

    @OnClick(R.id.button_renewal_proceed)
    public void setGoToPictureActivity(){
        RenewalOfCardDto renewalOfCardDto = (RenewalOfCardDto) getActivity().getIntent().getExtras().getSerializable("application");
        renewalOfCardDto.setIssuingAuthority(issuingAuthority.getText().toString());
        renewalOfCardDto.setTachographCardNumber(tachographCardNumber.getText().toString());
        renewalOfCardDto.setCountryOfIssuing(EUCountry.valueOf(countryIssuing.getSelectedItem().toString().toUpperCase()));
        renewalOfCardDto.setRenewalOfCardReason(RenewalOfCardReason.valueOf(reason.getSelectedItem().toString().toUpperCase()));
        renewalOfCardDto.setDateOfExpir(new Date(12332));
        mNavigator.navigateToPersonalDetails(renewalOfCardDto);
    }
}
