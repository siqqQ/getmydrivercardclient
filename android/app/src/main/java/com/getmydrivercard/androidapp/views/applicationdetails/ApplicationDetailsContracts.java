package com.getmydrivercard.androidapp.views.applicationdetails;

public interface ApplicationDetailsContracts {
    interface View {

        void setPresenter(ApplicationDetailsContracts.Presenter presenter);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(ApplicationDetailsContracts.View view);

        void unsubscribe();
    }

    interface Navigator {
        void navigateToAdmin();
        void navigateToMenu();
    }
}
