package com.getmydrivercard.androidapp.views.exchange;

import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;

public interface ExchangeContracts {
    interface View {

        void setPresenter(ExchangeContracts.Presenter presenter);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(ExchangeContracts.View view);

        void unsubscribe();
    }

    interface Navigator {

        void navigateToPictures(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto);
    }
}
