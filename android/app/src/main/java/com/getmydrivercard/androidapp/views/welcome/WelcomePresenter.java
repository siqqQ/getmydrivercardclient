package com.getmydrivercard.androidapp.views.welcome;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;

public class WelcomePresenter implements WelcomeContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private WelcomeContracts.View mView;

    @Inject
    public WelcomePresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(WelcomeContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }
}
