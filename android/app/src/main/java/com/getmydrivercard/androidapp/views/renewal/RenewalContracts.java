package com.getmydrivercard.androidapp.views.renewal;

import com.getmydrivercard.androidapp.models.RenewalOfCardDto;

public interface RenewalContracts {
    interface View {

        void setPresenter(RenewalContracts.Presenter presenter);

        void navigateToPersonalDetails(RenewalOfCardDto renewalOfCardDto);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(RenewalContracts.View view);

        void unsubscribe();
    }

    interface Navigator {

        void navigateToPersonalDetails(RenewalOfCardDto renewalOfCardDto);
    }
}
