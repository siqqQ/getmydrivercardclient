package com.getmydrivercard.androidapp.views.menu;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;

import javax.inject.Inject;

public class MenuPresenter implements MenuContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private MenuContracts.View mView;

    @Inject
    public MenuPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(MenuContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }

}
