package com.getmydrivercard.androidapp.views.myapplications;


import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;
import com.getmydrivercard.androidapp.models.ApplicationsDetailsDto;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.DrivingLicenseDto;
import com.getmydrivercard.androidapp.models.NewTachographCardSpecial;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.PhotoDto;
import com.getmydrivercard.androidapp.views.menu.MenuContracts;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyApplicationsFragment extends Fragment implements MyApplicationsContracts.View {


    private MyApplicationsContracts.Presenter mPresenter;

    private MyApplicationsContracts.Navigator mNavigator;

    @Inject
    public MyApplicationsFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.listView_myApplications_myApplications)
    ListView lv;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_applications, container, false);
        OkHttpHttpRequester example = new OkHttpHttpRequester();
        ResponseBody response = null;
        //GET TOKEN:
        ResponseBody response2 = example.getToken("bobi", "123456");
        String token = null;
        String response3 = null;
        try {
            response3 = response2.string();
            token = response3.substring(17, response3.indexOf("\",\"token_type"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        response = example.getMyApplications(token);
        String responseString = null;
        try {
            responseString = response.string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<ApplicationsDetailsDto>>() {}.getType();
        ArrayList<ApplicationsDetailsDto> applicationsDetailsDtos = new Gson().fromJson(responseString, listType);
        List<String> textToVisualize = new ArrayList<>();
        for (int i = 0; i < applicationsDetailsDtos.size(); i++) {
            textToVisualize.add(applicationsDetailsDtos.get(i).getApplicationType() + " " + applicationsDetailsDtos.get(i).getApplicationStatus().toString());
        }
        lv = (ListView) view.findViewById(R.id.listView_myApplications_myApplications);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getContext(),
                android.R.layout.simple_list_item_1,
                textToVisualize );

        lv.setAdapter(arrayAdapter);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(MyApplicationsContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(MyApplicationsContracts.Navigator navigator) {
        mNavigator = navigator;
    }
}
