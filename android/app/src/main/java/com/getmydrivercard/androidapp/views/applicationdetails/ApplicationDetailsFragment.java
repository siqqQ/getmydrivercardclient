package com.getmydrivercard.androidapp.views.applicationdetails;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationStatus;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;
import com.getmydrivercard.androidapp.models.ApplicationsDetailsDto;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ChangeApplicationStatusDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.views.admin.AdminActivity;
import com.getmydrivercard.androidapp.views.menu.MenuActivity;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApplicationDetailsFragment extends Fragment implements ApplicationDetailsContracts.View {


    private ApplicationDetailsContracts.Presenter mPresenter;

    private ApplicationDetailsContracts.Navigator mNavigator;

    @Inject
    public ApplicationDetailsFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.textView_applicationDetails_approve)
    Button approve;

    @BindView(R.id.textView_applicationDetails_reject)
    Button reject;

    @BindView(R.id.textView_applicationDetails_save)
    Button save;

    @BindView(R.id.textView_applicationDetails_noSave)
    Button noSave;

    @BindView(R.id.textView_applicationDetails_applicationType)
    TextView applicationType;

    @BindView(R.id.textView_applicationDetails_applicationStatus)
    TextView applicaitonStatus;

    @BindView(R.id.textView_applicationDetails_egn)
    TextView egn;
    @BindView(R.id.textView_applicationDetails_firstName)
    TextView firstName;
    @BindView(R.id.textView_applicationDetails_middleNames)
    TextView middleNames;
    @BindView(R.id.textView_applicationDetails_surname)
    TextView surname;
    @BindView(R.id.textView_applicationDetails_dateOfBirth)
    TextView dateOfBirth;
    @BindView(R.id.textView_applicationDetails_email)
    TextView email;
    @BindView(R.id.textView_applicationDetails_addressCountry)
    TextView addressCountry;
    @BindView(R.id.textView_applicationDetails_addressArea)
    TextView addressArea;
    @BindView(R.id.textView_applicationDetails_addressCity)
    TextView addressCity;
    @BindView(R.id.textView_applicationDetails_addressStreet)
    TextView addressStreet;
    @BindView(R.id.textView_applicationDetails_phoneCountry)
    TextView phoneCountry;
    @BindView(R.id.textView_applicationDetails_phonePhone)
    TextView phonePhone;
    @BindView(R.id.imageView_applicationDetails_selfiePhoto)
    ImageView selfiePhoto;
    @BindView(R.id.imageView_applicationDetails_idCardPhoto)
    ImageView idCardPhoto;
    @BindView(R.id.imageView_applicationDetails_drivingLicensePhoto)
    ImageView drivingLicensePhoto;
    @BindView(R.id.imageView_applicationDetails_signatureForDeclarationPhoto)
    ImageView signatureForDeclarationPhoto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_application_details, container, false);
        ApplicationsDetailsDto applicationsDetailsDto = (ApplicationsDetailsDto) getActivity().getIntent().getExtras().getSerializable("aplicationDetails");
        ButterKnife.bind(this, view);
        applicationType.setText("Application Type: " + String.valueOf(applicationsDetailsDto.getApplicationType()));
        applicaitonStatus.setText("Application Status: " + String.valueOf(applicationsDetailsDto.getApplicationStatus()));
        egn.setText("EGN: " + applicationsDetailsDto.getPersonalDetailsDto().getEgn());
        firstName.setText("First Name: " + applicationsDetailsDto.getPersonalDetailsDto().getFirstName());
        middleNames.setText("Middle Names: " + applicationsDetailsDto.getPersonalDetailsDto().getMiddleNames());
        surname.setText("Surname: " + applicationsDetailsDto.getPersonalDetailsDto().getSurname());
        Date dayOfBirthDate = applicationsDetailsDto.getPersonalDetailsDto().getDateOfBirth();
        dateOfBirth.setText("Date Of Birth: " + dayOfBirthDate.getDay() + "/" + dayOfBirthDate.getMonth() + "/" + dayOfBirthDate.getYear());
        email.setText("Email: " + applicationsDetailsDto.getPersonalDetailsDto().getEmail());
        addressCountry.setText("Country: " + String.valueOf(applicationsDetailsDto.getPersonalDetailsDto().getAddressDto().getCountry()));
        addressArea.setText("Area: " + applicationsDetailsDto.getPersonalDetailsDto().getAddressDto().getArea());
        addressCity.setText("City: " + applicationsDetailsDto.getPersonalDetailsDto().getAddressDto().getCity());
        addressStreet.setText("Street: " + applicationsDetailsDto.getPersonalDetailsDto().getAddressDto().getStreet());
        phoneCountry.setText("Country: " + String.valueOf(applicationsDetailsDto.getPersonalDetailsDto().getPhoneNumberDto().getCountryNumber()));
        phonePhone.setText("Number: " + applicationsDetailsDto.getPersonalDetailsDto().getPhoneNumberDto().getNumber());
        selfiePhoto.setImageBitmap(StringToBitMap(applicationsDetailsDto.getSelfiePhoto().getPicture()));
        idCardPhoto.setImageBitmap(StringToBitMap(applicationsDetailsDto.getIdCardPhoto().getPicture()));
        drivingLicensePhoto.setImageBitmap(StringToBitMap(applicationsDetailsDto.getDrivingLicensePhoto().getPicture()));
        signatureForDeclarationPhoto.setImageBitmap(StringToBitMap(applicationsDetailsDto.getSignatureForDeclarationPhoto().getPicture()));
        if (applicationsDetailsDto.getRecordId() == null){
            approve.setVisibility(View.INVISIBLE);
            reject.setVisibility(View.INVISIBLE);
            //save.setVisibility(View.VISIBLE);
        }

        return view;
    }

    public Bitmap StringToBitMap(String encodedString){
        try {
            byte [] encodeByte= Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap= BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch(Exception e) {
            e.getMessage();
            return null;
        }
    }

    @OnClick(R.id.textView_applicationDetails_approve)
    public void approve(){
        OkHttpHttpRequester okHttpHttpRequester = new OkHttpHttpRequester();
        ResponseBody response = null;
        ResponseBody response2 = okHttpHttpRequester.getToken("admin", "123456");
        String token = null;
        String response3 = null;
        try {
            response3 = response2.string();
            token = response3.substring(17, response3.indexOf("\",\"token_type"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ApplicationsDetailsDto applicationsDetailsDto = (ApplicationsDetailsDto) getActivity().getIntent().getExtras().getSerializable("aplicationDetails");
        ChangeApplicationStatusDto changeApplicationStatusDto = new ChangeApplicationStatusDto(ApplicationStatus.APPROVED, applicationsDetailsDto.getRecordId());
        response = okHttpHttpRequester.changeApplicationStatus(changeApplicationStatusDto, token);
        String responseString = null;
        try {
            responseString = response.string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        okHttpHttpRequester.sendNotification();
        mNavigator.navigateToAdmin();
    }

    @OnClick(R.id.textView_applicationDetails_reject)
    public void reject(){
        OkHttpHttpRequester okHttpHttpRequester = new OkHttpHttpRequester();
        ResponseBody response = null;
        ResponseBody response2 = okHttpHttpRequester.getToken("admin", "123456");
        String token = null;
        String response3 = null;
        try {
            response3 = response2.string();
            token = response3.substring(17, response3.indexOf("\",\"token_type"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ApplicationsDetailsDto applicationsDetailsDto = (ApplicationsDetailsDto) getActivity().getIntent().getExtras().getSerializable("aplicationDetails");
        ChangeApplicationStatusDto changeApplicationStatusDto = new ChangeApplicationStatusDto(ApplicationStatus.REJECTED, applicationsDetailsDto.getRecordId());
        response = okHttpHttpRequester.changeApplicationStatus(changeApplicationStatusDto, token);
        String responseString = null;
        try {
            responseString = response.string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        okHttpHttpRequester.sendNotification();
        mNavigator.navigateToAdmin();
    }

    @OnClick(R.id.textView_applicationDetails_noSave)
    public void noSave(){
        mNavigator.navigateToMenu();
    }

    @OnClick(R.id.textView_applicationDetails_save)
    public void save(){
        OkHttpHttpRequester okHttpHttpRequester = new OkHttpHttpRequester();
        ApplicationsDetailsDto applicationsDetailsDto = (ApplicationsDetailsDto) getActivity().getIntent().getExtras().getSerializable("aplicationDetails");
        if (applicationsDetailsDto.getApplicationType() == ApplicationType.NEW){
            okHttpHttpRequester.addApplicationForNewCard(new ApplyingForFirstDigitalTachographCardDto(ApplicationType.NEW, ApplicationStatus.PENDING, applicationsDetailsDto.getPersonalDetailsDto(),
                    applicationsDetailsDto.getSelfiePhoto(), applicationsDetailsDto.getIdCardPhoto(), applicationsDetailsDto.getDrivingLicensePhoto(), applicationsDetailsDto.getSignatureForDeclarationPhoto(),
                    applicationsDetailsDto.getDrivingLicenseDto()), getActivity().getIntent().getExtras().get("token").toString());
        }
        else if (applicationsDetailsDto.getApplicationType() == ApplicationType.RENEWAL){
            okHttpHttpRequester.addApplicationRenewal(new RenewalOfCardDto(applicationsDetailsDto.getPersonalDetailsDto(), applicationsDetailsDto.getSelfiePhoto(),applicationsDetailsDto.getIdCardPhoto(),
                    applicationsDetailsDto.getDrivingLicensePhoto(), applicationsDetailsDto.getSignatureForDeclarationPhoto(), applicationsDetailsDto.getPreviousTachographCardPhoto(),
                    applicationsDetailsDto.getRenewalOfCardReason(), applicationsDetailsDto.getEUCountryOfIssuing(), applicationsDetailsDto.getIssuingAuthority(), applicationsDetailsDto.getTachographCardNumber(),
                    applicationsDetailsDto.getDateOfExprity()), getActivity().getIntent().getExtras().get("token").toString());
        }
        else if (applicationsDetailsDto.getApplicationType() == ApplicationType.REPLACEMENT){
            okHttpHttpRequester.addApplicationReplacement(new ReplacementOfCardDto(applicationsDetailsDto.getPersonalDetailsDto(), applicationsDetailsDto.getSelfiePhoto(), applicationsDetailsDto.getIdCardPhoto(),
                    applicationsDetailsDto.getDrivingLicensePhoto(), applicationsDetailsDto.getSignatureForDeclarationPhoto(), applicationsDetailsDto.getPreviousTachographCardPhoto(),
                    applicationsDetailsDto.getReplacementOfCardReason(), applicationsDetailsDto.getEUCountryOfIssuing(), applicationsDetailsDto.getIssuingAuthority(), applicationsDetailsDto.getTachographCardNumber(),
                    applicationsDetailsDto.getDateOfExprity()), getActivity().getIntent().getExtras().get("token").toString());
        }
        else if (applicationsDetailsDto.getApplicationType() == ApplicationType.EXCHANGE){
            okHttpHttpRequester.addApplicationExchange(new ExchangingEUCardForBGCardDto(ApplicationType.EXCHANGE, ApplicationStatus.PENDING, applicationsDetailsDto.getPersonalDetailsDto(), applicationsDetailsDto.getSelfiePhoto(), applicationsDetailsDto.getIdCardPhoto(),
                    applicationsDetailsDto.getDrivingLicensePhoto(), applicationsDetailsDto.getSignatureForDeclarationPhoto(), applicationsDetailsDto.getPreviousTachographCardPhoto(),
                    applicationsDetailsDto.getEUCountryOfIssuing(), applicationsDetailsDto.getTachographCardNumber(), applicationsDetailsDto.getCountryOfIssuingOfDrivingLicense(),
                    applicationsDetailsDto.getDrivingLicenseNumber()), getActivity().getIntent().getExtras().get("token").toString());
        }
        mNavigator.navigateToMenu();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(ApplicationDetailsContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(ApplicationDetailsContracts.Navigator navigator) {
        mNavigator = navigator;
    }
}
