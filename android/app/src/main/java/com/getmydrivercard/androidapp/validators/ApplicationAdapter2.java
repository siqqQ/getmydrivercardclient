package com.getmydrivercard.androidapp.validators;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.models.ApplicationListView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ApplicationAdapter2 extends RecyclerView.Adapter<ApplicationAdapter2.SuperheroViewHolder> {
    private List<ApplicationListView> mSuperheroes;
    private OnSuperheroClickListener mOnSuperheroClickListener;

    @Inject
    public ApplicationAdapter2() {
        mSuperheroes = new ArrayList<>();
    }

    @NonNull
    @Override
    public SuperheroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row, parent, false);
        int height = parent.getMeasuredHeight() / 3;
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        lp.height = height;
        view.setLayoutParams(lp);
        view.setMinimumHeight(height);
        return new SuperheroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuperheroViewHolder holder, int position) {
        holder.setOnClickListener(mOnSuperheroClickListener);
        holder.bind(mSuperheroes.get(position));
    }

    @Override
    public int getItemCount() {
        return mSuperheroes.size();
    }

    public ApplicationListView getItem(int position) {
        return mSuperheroes.get(position);
    }

    public void clear() {
        mSuperheroes.clear();
    }

    public void addAll(List<ApplicationListView> superheroes) {
        mSuperheroes.addAll(superheroes);
    }

    public void setOnSuperheroClickListener(OnSuperheroClickListener onSuperheroClickListener) {
        this.mOnSuperheroClickListener = onSuperheroClickListener;
    }

    public static class SuperheroViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.row_status)
        TextView row_starus;

        @BindView(R.id.row_type)
        TextView row_type;

        @BindView(R.id.row_user)
        TextView row_user;
        private OnSuperheroClickListener mOnClickListener;
        private ApplicationListView mSuperhero;

        SuperheroViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        void bind(ApplicationListView superhero) {
            row_user.setText(superhero.getUsername());
            row_starus.setText(superhero.getApplicationStatus());
            mSuperhero = superhero;
        }

        @OnClick
        public void onClick() {
            mOnClickListener.onClick(mSuperhero);
        }

        public void setOnClickListener(OnSuperheroClickListener onClickListener) {
            mOnClickListener = onClickListener;
        }
    }

    interface OnSuperheroClickListener {
        void onClick(ApplicationListView superhero);
    }
}
