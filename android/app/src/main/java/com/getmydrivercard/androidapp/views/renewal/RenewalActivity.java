package com.getmydrivercard.androidapp.views.renewal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.models.CurrentPicture;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.pictures.PicturesActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class RenewalActivity extends BaseDrawerActivity implements RenewalContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    RenewalFragment mView;

    @Inject
    RenewalContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renewal);
        ButterKnife.bind(this);
        mView.setPresenter(mPresenter);
        mView.setNavigator(this);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPersonalDetails(RenewalOfCardDto renewalOfCardDto) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
        intent.putExtra("applicationType", ApplicationType.RENEWAL);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("application", renewalOfCardDto);
        intent.putExtra("personalDetails", getIntent().getExtras().getSerializable("personalDetails"));
        startActivity(intent);
        finish();
    }

}
