package com.getmydrivercard.androidapp.enums;

public enum UserRole {
    ADMIN, USER
}
