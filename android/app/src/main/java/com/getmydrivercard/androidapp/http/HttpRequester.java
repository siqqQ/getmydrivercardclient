package com.getmydrivercard.androidapp.http;

import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ChangeApplicationStatusDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.models.UserDto;

import java.io.IOException;

import okhttp3.ResponseBody;

public interface HttpRequester {
    String get(String url) throws IOException;

    String post(String url, String body) throws IOException;

    ResponseBody createNewUser(UserDto userDto) throws IOException;

    ResponseBody savePersonalDetails(PersonalDetailsDto personalDetailsDto, String token);

    ResponseBody getToken(String username, String password);

    ResponseBody addApplicationForNewCard(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto, String token);

    ResponseBody getMyApplications(String token);

    ResponseBody getUser(String token);

    ResponseBody getAllApplications(String token);

    ResponseBody changeApplicationStatus(ChangeApplicationStatusDto changeApplicationStatusDto, String token);

    ResponseBody addApplicationExchange(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto, String token);

    ResponseBody addApplicationReplacement(ReplacementOfCardDto replacementOfCardDto, String token);

    ResponseBody addApplicationRenewal(RenewalOfCardDto renewalOfCardDto, String token);

    ResponseBody sendNotification();
}
