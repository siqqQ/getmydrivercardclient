package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.async.AsyncSchedulerProvider;
import com.getmydrivercard.androidapp.async.base.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AsyncModule {
    @Provides
    @Singleton
    public SchedulerProvider schedulerProvider() {
        return AsyncSchedulerProvider.getInstance();
    }
}
