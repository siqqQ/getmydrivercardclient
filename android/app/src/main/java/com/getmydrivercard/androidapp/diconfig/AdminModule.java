package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.admin.AdminContracts;
import com.getmydrivercard.androidapp.views.admin.AdminFragment;
import com.getmydrivercard.androidapp.views.admin.AdminPresenter;
import com.getmydrivercard.androidapp.views.login.LoginContracts;
import com.getmydrivercard.androidapp.views.login.LoginFragment;
import com.getmydrivercard.androidapp.views.login.LoginPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AdminModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract AdminFragment adminFragment();

    @ActivityScoped
    @Binds
    abstract AdminContracts.Presenter adminPresenter(AdminPresenter presenter);
}