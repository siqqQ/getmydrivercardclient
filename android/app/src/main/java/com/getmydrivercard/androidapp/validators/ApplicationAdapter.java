package com.getmydrivercard.androidapp.validators;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationStatus;
import com.getmydrivercard.androidapp.models.ApplicationListView;

import java.util.ArrayList;

public class ApplicationAdapter extends BaseAdapter implements Filterable {

    private ArrayList<ApplicationListView> mOriginalValues; // Original Values
    private ArrayList<ApplicationListView> mDisplayedValues;    // Values to be displayed
    LayoutInflater inflater;

    public ApplicationAdapter(Context context, ArrayList<ApplicationListView> mProductArrayList) {
        this.mOriginalValues = mProductArrayList;
        this.mDisplayedValues = mProductArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mDisplayedValues.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        LinearLayout llContainer;
        TextView type, status, user;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row, null);
            holder.llContainer = (LinearLayout) convertView.findViewById(R.id.llContainer);
            holder.type = (TextView) convertView.findViewById(R.id.row_type);
            holder.status = (TextView) convertView.findViewById(R.id.row_status);
            holder.user = (TextView) convertView.findViewById(R.id.row_user);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.type.setText(mDisplayedValues.get(position).getApplicationType());
        holder.status.setText(mDisplayedValues.get(position).getApplicationStatus() + "");
        holder.user.setText(mDisplayedValues.get(position).getUsername() + "");

//        holder.llContainer.setOnClickListener(new OnClickListener() {
//
//            public void onClick(View v) {
//
//                Toast.makeText(MainActivity.this, mDisplayedValues.get(position).name, Toast.LENGTH_SHORT).show();
//            }
//        });

        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                mDisplayedValues = (ArrayList<ApplicationListView>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<ApplicationListView> FilteredArrList = new ArrayList<ApplicationListView>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<ApplicationListView>(mDisplayedValues); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        String data = mOriginalValues.get(i).getUsername();
                        if (data.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(new ApplicationListView(mOriginalValues.get(i).getId(), mOriginalValues.get(i).getApplicationType(), mOriginalValues.get(i).getUsername(), mOriginalValues.get(i).getApplicationStatus()));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }
}

