package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.admin.AdminActivity;
import com.getmydrivercard.androidapp.views.applicationdetails.ApplicationDetailsActivity;
import com.getmydrivercard.androidapp.views.exchange.ExchangeActivity;
import com.getmydrivercard.androidapp.views.login.LoginActivity;
import com.getmydrivercard.androidapp.views.menu.MenuActivity;
import com.getmydrivercard.androidapp.views.myapplications.MyApplicationsActivity;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardActivity;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsActivity;
import com.getmydrivercard.androidapp.views.personaldetails2.PersonalDetails2Activity;
import com.getmydrivercard.androidapp.views.pictures.PicturesActivity;
import com.getmydrivercard.androidapp.views.register.RegisterActivity;
import com.getmydrivercard.androidapp.views.renewal.RenewalActivity;
import com.getmydrivercard.androidapp.views.replacement.ReplacementActivity;
import com.getmydrivercard.androidapp.views.welcome.WelcomeActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = RegisterModule.class
    )
    abstract RegisterActivity registerActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = WelcomeModule.class
    )
    abstract WelcomeActivity welcomeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = PersonalDetailsModule.class
    )
    abstract PersonalDetailsActivity personalDetailsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = MenuModule.class
    )
    abstract MenuActivity menuActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = NewTahographCardModule.class
    )
    abstract NewTahographCardActivity newTahographCardActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = PicturesModule.class
    )
    abstract PicturesActivity picturesActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = LoginModule.class
    )
    abstract LoginActivity loginActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = MyApplicationsModule.class
    )
    abstract MyApplicationsActivity myApplicationsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = AdminModule.class
    )
    abstract AdminActivity adminActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = ApplicationDetailsModule.class
    )
    abstract ApplicationDetailsActivity applicationDetailsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = PersonalDetails2Module.class
    )
    abstract PersonalDetails2Activity personalDetails2Activity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = ExchangeModule.class
    )
    abstract ExchangeActivity exchangeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = ReplacementModule.class
    )
    abstract ReplacementActivity replacementActivity();

    @ActivityScoped
    @ContributesAndroidInjector(
            modules = RenewalModule.class
    )
    abstract RenewalActivity renewalActivity();
}