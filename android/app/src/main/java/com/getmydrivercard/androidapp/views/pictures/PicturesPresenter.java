package com.getmydrivercard.androidapp.views.pictures;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;
import com.getmydrivercard.androidapp.views.register.RegisterContracts;

import javax.inject.Inject;

public class PicturesPresenter implements PicturesContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private PicturesContracts.View mView;

    @Inject
    public PicturesPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(PicturesContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }
}
