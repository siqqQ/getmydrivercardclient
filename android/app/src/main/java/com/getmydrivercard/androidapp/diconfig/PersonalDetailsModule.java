package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsContracts;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsFragment;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PersonalDetailsModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract PersonalDetailsFragment personalDetailsFragment();

    @ActivityScoped
    @Binds
    abstract PersonalDetailsContracts.Presenter personalDetailsPresenter(PersonalDetailsPresenter presenter);
}
