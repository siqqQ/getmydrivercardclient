package com.getmydrivercard.androidapp.views.newtahographcard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.CurrentPicture;
import com.getmydrivercard.androidapp.models.NewTachographCardSpecial;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.pictures.PicturesActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class NewTahographCardActivity extends BaseDrawerActivity implements NewTahographCardContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    NewTahographCardFragment mView;

    @Inject
    NewTahographCardContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_tahograph_card);
        ButterKnife.bind(this);
        mView.setPresenter(mPresenter);
        mView.setNavigator(this);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPersonalDetails(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
        intent.putExtra("applicationType", ApplicationType.NEW);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("application", applyingForFirstDigitalTachographCardDto);
        intent.putExtra("personalDetails", getIntent().getExtras().getSerializable("personalDetails"));
        startActivity(intent);
        finish();
    }

}
