package com.getmydrivercard.androidapp.views.admin;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;

import javax.inject.Inject;

public class AdminPresenter implements AdminContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private AdminContracts.View mView;

    @Inject
    public AdminPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(AdminContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }

}