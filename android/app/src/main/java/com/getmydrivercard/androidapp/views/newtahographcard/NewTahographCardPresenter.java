package com.getmydrivercard.androidapp.views.newtahographcard;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;

import javax.inject.Inject;

public class NewTahographCardPresenter implements NewTahographCardContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private NewTahographCardContracts.View mView;

    @Inject
    public NewTahographCardPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(NewTahographCardContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }
}
