package com.getmydrivercard.androidapp.views.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.PreviousActivity;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.admin.AdminActivity;
import com.getmydrivercard.androidapp.views.menu.MenuActivity;
import com.getmydrivercard.androidapp.views.menu.MenuContracts;
import com.getmydrivercard.androidapp.views.menu.MenuFragment;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class LoginActivity extends BaseDrawerActivity implements LoginContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    LoginFragment mView;

    @Inject
    LoginContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mView.setPresenter(mPresenter);
        mView.setNavigator(this);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToMenu(String token, String role) {
        if (role.equals("user")){
            Intent intent = new Intent(this, MenuActivity.class);
            intent.putExtra("PreviousActivity", PreviousActivity.REGISTER);
            intent.putExtra("token", token);
            startActivity(intent);
            finish();
        }
        else {
            Intent intent = new Intent(this, AdminActivity.class);
            intent.putExtra("PreviousActivity", PreviousActivity.REGISTER);
            intent.putExtra("token", token);
            startActivity(intent);
            finish();
        }
    }
}
