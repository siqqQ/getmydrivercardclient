package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.myapplications.MyApplicationsContracts;
import com.getmydrivercard.androidapp.views.myapplications.MyApplicationsFragment;
import com.getmydrivercard.androidapp.views.myapplications.MyApplicationsPresenter;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardContracts;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardFragment;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MyApplicationsModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract MyApplicationsFragment myApplicationsFragment();

    @ActivityScoped
    @Binds
    abstract MyApplicationsContracts.Presenter myApplicationsPresenter(MyApplicationsPresenter presenter);
}

