package com.getmydrivercard.androidapp.views.exchange;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.DrivingLicenseCategory;
import com.getmydrivercard.androidapp.models.DrivingLicenseDto;
import com.getmydrivercard.androidapp.models.EUCountry;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.NewTachographCardSpecial;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardContracts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExchangeFragment extends Fragment implements ExchangeContracts.View {


    private ExchangeContracts.Presenter mPresenter;

    private ExchangeContracts.Navigator mNavigator;

    @BindView(R.id.spinner_exchange_countryIssuing)
    Spinner countryOfIssuing;

    @BindView(R.id.spinner_exchange_countryIssuingCard)
    Spinner countryIssuingCard;

    @BindView(R.id.editText_exchange_drivingLicenseNumber)
    EditText drivingLicenseNumber;

    @BindView(R.id.editText_exchange_tachographCardNumber)
    EditText tachographCardNumber;

    @Inject
    public ExchangeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exchange, container, false);
        drivingLicenseNumber = view.findViewById(R.id.editText_exchange_drivingLicenseNumber);
        drivingLicenseNumber.setText("2434424");
        tachographCardNumber = view.findViewById(R.id.editText_exchange_tachographCardNumber);
        tachographCardNumber.setText("43244423");
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(ExchangeContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(ExchangeContracts.Navigator navigator) {
        mNavigator = navigator;
    }

    @OnClick(R.id.button_exchange_proceed)
    public void proceed(){
        ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto = (ExchangingEUCardForBGCardDto)getActivity().getIntent().getExtras().getSerializable("application");
        countryOfIssuing = getActivity().findViewById(R.id.spinner_exchange_countryIssuing);
        exchangingEUCardForBGCardDto.setEUCountryOfIssuing(EUCountry.valueOf(countryOfIssuing.getSelectedItem().toString().toUpperCase()));
        countryIssuingCard = getActivity().findViewById(R.id.spinner_exchange_countryIssuingCard);
        exchangingEUCardForBGCardDto.setCountryOfIssuingOfDrivingLicense(EUCountry.valueOf(countryIssuingCard.getSelectedItem().toString().toUpperCase()));
        drivingLicenseNumber = getActivity().findViewById(R.id.editText_exchange_drivingLicenseNumber);
        exchangingEUCardForBGCardDto.setDrivingLicenseNumber(drivingLicenseNumber.toString());
        tachographCardNumber = getActivity().findViewById(R.id.editText_exchange_tachographCardNumber);
        exchangingEUCardForBGCardDto.setTachographCardNumber(tachographCardNumber.toString());
        mNavigator.navigateToPictures(exchangingEUCardForBGCardDto);
    }

}
