package com.getmydrivercard.androidapp.views.exchange;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;
import com.getmydrivercard.androidapp.views.login.LoginContracts;

import javax.inject.Inject;

public class ExchangePresenter implements ExchangeContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private ExchangeContracts.View mView;

    @Inject
    public ExchangePresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(ExchangeContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }

}

