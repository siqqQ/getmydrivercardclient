package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsContracts;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsFragment;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsPresenter;
import com.getmydrivercard.androidapp.views.personaldetails2.PersonalDetails2Contracts;
import com.getmydrivercard.androidapp.views.personaldetails2.PersonalDetails2Fragment;
import com.getmydrivercard.androidapp.views.personaldetails2.PersonalDetails2Presenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PersonalDetails2Module {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract PersonalDetails2Fragment personalDetails2Fragment();

    @ActivityScoped
    @Binds
    abstract PersonalDetails2Contracts.Presenter personalDetails2Presenter(PersonalDetails2Presenter presenter);
}
