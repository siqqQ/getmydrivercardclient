package com.getmydrivercard.androidapp.views.replacement;

import android.content.Intent;
import android.os.Bundle;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.models.CurrentPicture;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.pictures.PicturesActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class ReplacementActivity extends BaseDrawerActivity implements ReplacementContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    ReplacementFragment mView;

    @Inject
    ReplacementContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replacement);
        ButterKnife.bind(this);
        mView.setPresenter(mPresenter);
        mView.setNavigator(this);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPersonalDetails(ReplacementOfCardDto replacementOfCardDto) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
        intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("application", replacementOfCardDto);
        intent.putExtra("personalDetails", getIntent().getExtras().getSerializable("personalDetails"));
        startActivity(intent);
        finish();
    }

}
