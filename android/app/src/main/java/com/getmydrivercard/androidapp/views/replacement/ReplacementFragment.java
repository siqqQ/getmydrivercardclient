package com.getmydrivercard.androidapp.views.replacement;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ReplacementOfCardReason;
import com.getmydrivercard.androidapp.models.EUCountry;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;

import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReplacementFragment extends Fragment implements ReplacementContracts.View {


    private ReplacementContracts.Presenter mPresenter;

    private ReplacementContracts.Navigator mNavigator;

    @BindView(R.id.editText_replacement_issuingAuthority)
    EditText issuingAuthority;

    @BindView(R.id.button_replacement_proceed)
    Button proceed;

    @BindView(R.id.editText_replacement_tachographCardNumber)
    EditText tachographCardNumber;

    @BindView(R.id.spinner_replacement_countryIssuing)
    Spinner countryIssuing;

    @BindView(R.id.spinner_replacement_reason)
    Spinner reason;

    @BindView(R.id.textView_replacement_dateOfExpirityCurrentCard)
    TextView dateOfExpirityCurrentCard;

    @Inject
    public ReplacementFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_replacement, container, false);
        ButterKnife.bind(this, view);
        issuingAuthority.setText("MVR");
        tachographCardNumber.setText("132221321");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(ReplacementContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void navigateToPersonalDetails(ReplacementOfCardDto replacementOfCardDto) {
        mNavigator.navigateToPersonalDetails(replacementOfCardDto);
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(ReplacementContracts.Navigator navigator) {
        mNavigator = navigator;
    }

    @OnClick(R.id.button_replacement_proceed)
    public void setGoToPictureActivity(){
        ReplacementOfCardDto replacementOfCardDto = (ReplacementOfCardDto) getActivity().getIntent().getExtras().getSerializable("application");
        replacementOfCardDto.setIssuingAuthority(issuingAuthority.getText().toString());
        replacementOfCardDto.setTachographCardNumber(tachographCardNumber.getText().toString());
        replacementOfCardDto.setCountryOfIssuing(EUCountry.valueOf(countryIssuing.getSelectedItem().toString().toUpperCase()));
        replacementOfCardDto.setReplacementOfCardReason(ReplacementOfCardReason.valueOf(reason.getSelectedItem().toString().toUpperCase()));
        replacementOfCardDto.setDateOfExpiry(new Date(12332));
        mNavigator.navigateToPersonalDetails(replacementOfCardDto);
    }
}
