package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.register.RegisterContracts;
import com.getmydrivercard.androidapp.views.register.RegisterFragment;
import com.getmydrivercard.androidapp.views.register.RegisterPresenter;
import com.getmydrivercard.androidapp.views.renewal.RenewalContracts;
import com.getmydrivercard.androidapp.views.renewal.RenewalFragment;
import com.getmydrivercard.androidapp.views.renewal.RenewalPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class RenewalModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract RenewalFragment renewalFragment();

    @ActivityScoped
    @Binds
    abstract RenewalContracts.Presenter renewalPresenter(RenewalPresenter presenter);
}
