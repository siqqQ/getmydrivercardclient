package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.applicationdetails.ApplicationDetailsContracts;
import com.getmydrivercard.androidapp.views.applicationdetails.ApplicationDetailsFragment;
import com.getmydrivercard.androidapp.views.applicationdetails.ApplicationDetailsPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ApplicationDetailsModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract ApplicationDetailsFragment applicationDetailsFragment();

    @ActivityScoped
    @Binds
    abstract ApplicationDetailsContracts.Presenter applicationDetailsContracts(ApplicationDetailsPresenter presenter);
}
