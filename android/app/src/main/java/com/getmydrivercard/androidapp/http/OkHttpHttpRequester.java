package com.getmydrivercard.androidapp.http;

import android.os.StrictMode;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ChangeApplicationStatusDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.models.UserDto;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OkHttpHttpRequester implements HttpRequester {
    public OkHttpHttpRequester() {

    }

    //private String host = "http://192.168.0.100:8080";
    private String host = "http://10.0.2.2:8080";

    @Override
    public String get(String url) throws IOException {
        Request request = new Request.Builder()
                .get()
                .url(url)
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = client.newCall(request)
                .execute();

        String body = response.body().string();
        return body;
    }

    @Override
    public String post(String url, String bodyString) throws IOException {
        RequestBody body = RequestBody.create(
                MediaType.parse("application/json"),
                bodyString
        );

        Request request = new Request.Builder()
                .post(body)
                .url(url)
                .build();
        OkHttpClient client = new OkHttpClient();

        Response response = client.newCall(request)
                .execute();

        String responseBody = response.body().string();
        return responseBody;
    }



    @Override
    public ResponseBody createNewUser(UserDto userDto) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        String carAsString = objectMapper.writeValueAsString(userDto);
        RequestBody requestBody = RequestBody.create(JSON, carAsString);
//        RequestBody requestBody = new MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addFormDataPart("somParam", "someValue")
//                .build();
        Request request = new Request.Builder()
                .get()
                .post(requestBody)
                .url(host + "/user/new")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = client.newCall(request)
                .execute();



        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody savePersonalDetails(PersonalDetailsDto personalDetailsDto, String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        String carAsString = null;
        try {
            carAsString = objectMapper.writeValueAsString(personalDetailsDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(JSON, carAsString);
        Request request = new Request.Builder()
                .get()
                .put(requestBody)
                .addHeader("Authorization", "bearer " + token)
                .url(host + "/user/addPersonalDetails")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody changeApplicationStatus(ChangeApplicationStatusDto changeApplicationStatusDto, String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        String carAsString = null;
        try {
            carAsString = objectMapper.writeValueAsString(changeApplicationStatusDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(JSON, carAsString);
        Request request = new Request.Builder()
                .get()
                .addHeader("Authorization", "bearer " + token)
                .put(requestBody)
                .url(host + "/apply/changeStatus")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody getMyApplications(String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        Request request = new Request.Builder()
                .get()
                .addHeader("Authorization", "bearer " + token)
                .url(host + "/user/getApps")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody getToken(String username, String password) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/x-www-form-urlencoded");
        RequestBody requestBody2 = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", username)
                .addFormDataPart("password", password)
                .addFormDataPart("grant_type", "password")
                .build();
        Request request = new Request.Builder()
                .get()
                .header("Authorization", Credentials.basic("getmydrivercard", "bobi96"))
                .post(requestBody2)
                .url(host + "/oauth/token")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody getUser(String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        Request request = new Request.Builder()
                .get()
                .addHeader("Authorization", "bearer " + token)
                .url(host + "/user/getUser")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody sendNotification() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        Request request = new Request.Builder()
                .get()
                .url(host + "/send")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody getAllApplications(String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        Request request = new Request.Builder()
                .get()
                .addHeader("Authorization", "bearer " + token)
                .url(host + "/apply/getAll")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody addApplicationForNewCard(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto, String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        String carAsString = null;
        try {
            carAsString = objectMapper.writeValueAsString(applyingForFirstDigitalTachographCardDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(JSON, carAsString);
        Request request = new Request.Builder()
                .get()
                .addHeader("Authorization", "bearer " + token)
                .post(requestBody)
                .url(host + "/apply/new")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody addApplicationExchange(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto, String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        String carAsString = null;
        try {
            carAsString = objectMapper.writeValueAsString(exchangingEUCardForBGCardDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(JSON, carAsString);
        Request request = new Request.Builder()
                .get()
                .addHeader("Authorization", "bearer " + token)
                .post(requestBody)
                .url(host + "/apply/exchange")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody addApplicationReplacement(ReplacementOfCardDto replacementOfCardDto, String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        String carAsString = null;
        try {
            carAsString = objectMapper.writeValueAsString(replacementOfCardDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(JSON, carAsString);
        Request request = new Request.Builder()
                .get()
                .addHeader("Authorization", "bearer " + token)
                .post(requestBody)
                .url(host + "/apply/replacement")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

    @Override
    public ResponseBody addApplicationRenewal(RenewalOfCardDto renewalOfCardDto, String token) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        final MediaType JSON = MediaType.get("application/json; charset=utf-8");
        ObjectMapper objectMapper = new ObjectMapper();
        String carAsString = null;
        try {
            carAsString = objectMapper.writeValueAsString(renewalOfCardDto);
        } catch (IOException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(JSON, carAsString);
        Request request = new Request.Builder()
                .get()
                .addHeader("Authorization", "bearer " + token)
                .post(requestBody)
                .url(host + "/apply/renewal")
                .build();

        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseBody body2 = response.body();
        return body2;
    }

}
