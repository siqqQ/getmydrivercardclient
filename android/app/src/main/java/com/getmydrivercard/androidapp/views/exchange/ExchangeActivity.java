package com.getmydrivercard.androidapp.views.exchange;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.CurrentPicture;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardContracts;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardFragment;
import com.getmydrivercard.androidapp.views.pictures.PicturesActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class ExchangeActivity extends BaseDrawerActivity implements ExchangeContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    ExchangeFragment mView;

    @Inject
    ExchangeContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);
        ButterKnife.bind(this);
        mView.setPresenter(mPresenter);
        mView.setNavigator(this);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPictures(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("currentPicture", CurrentPicture.SELFIE_PHOTO);
        intent.putExtra("applicationType", ApplicationType.EXCHANGE);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("application", exchangingEUCardForBGCardDto);
        intent.putExtra("personalDetails", getIntent().getExtras().getSerializable("personalDetails"));
        startActivity(intent);
        finish();
    }

}
