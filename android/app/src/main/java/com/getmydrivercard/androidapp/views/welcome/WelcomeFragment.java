package com.getmydrivercard.androidapp.views.welcome;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getmydrivercard.androidapp.R;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeFragment extends Fragment implements WelcomeContracts.View {


    private WelcomeContracts.Presenter mPresenter;

//    @BindView(R.id.et_name)
//    EditText mNameEditText;

    //    @BindView(R.id.et_secret_identity)
//    EditText mSecretIdentity;
    private WelcomeContracts.Navigator mNavigator;

    @Inject
    public WelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @OnClick(R.id.button_welcome_login)
    public void goToLoginPage() {
        mNavigator.navigateToLoginPage();
    }

    @OnClick(R.id.button_welcome_register)
    public void goToRegisterPage() {
        mNavigator.navigateToRegisterPage();
    }

    @Override
    public void setPresenter(WelcomeContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(WelcomeContracts.Navigator navigator) {
        mNavigator = navigator;
    }

}
