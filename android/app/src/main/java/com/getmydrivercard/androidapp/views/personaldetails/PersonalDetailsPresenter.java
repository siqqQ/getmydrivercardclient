package com.getmydrivercard.androidapp.views.personaldetails;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;

import javax.inject.Inject;

public class PersonalDetailsPresenter implements PersonalDetailsContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private PersonalDetailsContracts.View mView;

    @Inject
    public PersonalDetailsPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(PersonalDetailsContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }
}
