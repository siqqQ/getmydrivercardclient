package com.getmydrivercard.androidapp.views.menu;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.getmydrivercard.androidapp.R;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment implements MenuContracts.View {


    private MenuContracts.Presenter mPresenter;

    private MenuContracts.Navigator mNavigator;

    @BindView(R.id.button_menu_new)
    Button newCard;

    @BindView(R.id.button_menu_exchange)
    Button exchange;

    @BindView(R.id.button_menu_replacement2)
    Button replacement2;

    @BindView(R.id.button_menu_renewal)
    Button renewal;

    @Inject
    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(MenuContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(MenuContracts.Navigator navigator) {
        mNavigator = navigator;
    }

    @OnClick(R.id.button_menu_new)
    public void navigateToPersonalDetails1(){
        mNavigator.navigateToPersonalDetailsNew();
    }

    @OnClick(R.id.button_menu_renewal)
    public void navigateToPersonalDetails2(){
        mNavigator.navigateToPersonalDetailsRenewal();
    }

    @OnClick(R.id.button_menu_replacement2)
    public void navigateToPersonalDetails3(){
        mNavigator.navigateToPersonalDetailsReplacement2();
    }

    @OnClick(R.id.button_menu_exchange)
    public void navigateToPersonalDetails5(){
        mNavigator.navigateToPersonalDetailsExchange();
    }
}
