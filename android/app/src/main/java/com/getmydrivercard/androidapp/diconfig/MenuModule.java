package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.menu.MenuContracts;
import com.getmydrivercard.androidapp.views.menu.MenuFragment;
import com.getmydrivercard.androidapp.views.menu.MenuPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MenuModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract MenuFragment menuFragment();

    @ActivityScoped
    @Binds
    abstract MenuContracts.Presenter menuPresenter(MenuPresenter presenter);
}
