package com.getmydrivercard.androidapp.uiutils;

public interface Navigator {
    void navigateWith(String superhero);
}
