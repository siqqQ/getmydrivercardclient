package com.getmydrivercard.androidapp.views.register;

import com.getmydrivercard.androidapp.async.base.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;

public class RegisterPresenter implements RegisterContracts.Presenter {
    private final SchedulerProvider mSchedulerProvider;
    private RegisterContracts.View mView;

    @Inject
    public RegisterPresenter(
            SchedulerProvider schedulerProvider) {
        mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void subscribe(RegisterContracts.View view) {
        mView = view;
    }

    @Override
    public void unsubscribe() {
        mView = null;
    }
}
