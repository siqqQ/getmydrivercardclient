package com.getmydrivercard.androidapp.views.newtahographcard;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;
import com.getmydrivercard.androidapp.models.AddressDto;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.DrivingLicenseCategory;
import com.getmydrivercard.androidapp.models.DrivingLicenseDto;
import com.getmydrivercard.androidapp.models.EUCountry;
import com.getmydrivercard.androidapp.models.NewTachographCardSpecial;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.PhoneNumberDto;
import com.getmydrivercard.androidapp.validators.Patterns;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewTahographCardFragment extends Fragment implements NewTahographCardContracts.View {


    private NewTahographCardContracts.Presenter mPresenter;

    private NewTahographCardContracts.Navigator mNavigator;

    @BindView(R.id.button_newTahographCard_goToPictureActivity)
    Button goToPictureActivity;

    @BindView(R.id.editText_newTahographCard_issuingAuthority)
    EditText issuingAuthority;

    @BindView(R.id.spinner_newTahographCard_drivingLicenseCategory)
    Spinner drivingLicenseCategory;

    @BindView(R.id.spinner_newTahographCard_issuingCountry)
    Spinner issuingCountry;

    @Inject
    public NewTahographCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_tahograph_card, container, false);
        ButterKnife.bind(this, view);
        issuingAuthority = view.findViewById(R.id.editText_newTahographCard_issuingAuthority);
        issuingAuthority.setText("MVR");
        issuingAuthority.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    issuingAuthority.setHint("");
                else
                    issuingAuthority.setHint("Write Your Card's Issuing Authority Here");
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public void setPresenter(NewTahographCardContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void navigateToPersonalDetails(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto) {
        mNavigator.navigateToPersonalDetails(applyingForFirstDigitalTachographCardDto);
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(NewTahographCardContracts.Navigator navigator) {
        mNavigator = navigator;
    }

    @OnClick(R.id.button_newTahographCard_goToPictureActivity)
    public void setGoToPictureActivity(){
//        Button goToPictureActivity;
//        EditText issuingAuthority;
//        Spinner drivingLicenseCategory;
//        Spinner issuingCountry;
        if (!Pattern.matches(Patterns.PATTERN_STRING_PLUS_SPACE, issuingAuthority.getText().toString())){
            issuingAuthority.setError("Issuing Authority Is Invalid");
        }
        else {
            ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto = (ApplyingForFirstDigitalTachographCardDto) getActivity().getIntent().getExtras().getSerializable("application");
            NewTachographCardSpecial special = new NewTachographCardSpecial(issuingAuthority.getText().toString(),  DrivingLicenseCategory.valueOf(drivingLicenseCategory.getSelectedItem().toString().toUpperCase()),  EUCountry.valueOf(issuingCountry.getSelectedItem().toString().toUpperCase()));
            applyingForFirstDigitalTachographCardDto.setDrivingLicenseDto(new DrivingLicenseDto(special.getIssuingCountry(), special.getDrivingLicenseCategory(), special.getIssuingAuthority()));
            mNavigator.navigateToPersonalDetails(applyingForFirstDigitalTachographCardDto);
        }
    }

}
