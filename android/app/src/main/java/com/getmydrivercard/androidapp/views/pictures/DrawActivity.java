package com.getmydrivercard.androidapp.views.pictures;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.models.CurrentPicture;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import butterknife.BindView;

//        super.onCreate(savedInstanceState);
//                setContentView(R.layout.activity_draw);
public class DrawActivity extends AppCompatActivity implements View.OnClickListener {

//    DrawingView dv ;
//    private Paint mPaint;
    private Button save;

    @BindView(R.id.canvas)
    CanvasView canvasView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);
        canvasView = (CanvasView)findViewById(R.id.canvas);
        save = findViewById(R.id.button_draw_savePicture);
        save.setOnClickListener(this);
//        dv = new DrawingView(this);
////        dv.setPadding(30, 30, 30, 30);
//        dv.setLayoutParams(new LinearLayout.LayoutParams(100, 100));
//        dv.setMinimumWidth(100);
//        setContentView(dv);
//        mPaint = new Paint();
//        mPaint.setAntiAlias(true);
//        mPaint.setDither(true);
//        mPaint.setColor(Color.GREEN);
//        mPaint.setStyle(Paint.Style.STROKE);
//        mPaint.setStrokeJoin(Paint.Join.ROUND);
//        mPaint.setStrokeCap(Paint.Cap.ROUND);
//        mPaint.setStrokeWidth(12);
    }

    public void clearCanvas(View v){
        canvasView.clearCanvas();
    }

    public CanvasView getCanvas(){
        return canvasView;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, PicturesActivity.class);
        intent.putExtra("currentPicture", CurrentPicture.SIGNATURE_FOR_DECLARATION_PHOTO);
        intent.putExtra("applicationType", ApplicationType.valueOf(getIntent().getExtras().get("applicationType").toString().toUpperCase()));
        intent.putExtra("application", getIntent().getExtras().getSerializable("application"));
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = loadBitmapFromView(canvasView);
        view.setDrawingCacheEnabled(false);
        String image = BitMapToString(bitmap);
        intent.putExtra("signature", image);
        startActivity(intent);
        finish();
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }
}
