package com.getmydrivercard.androidapp.views.personaldetails;


import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationStatus;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;
import com.getmydrivercard.androidapp.models.AddressDto;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.EUCountry;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.PersonalDetailsDto;
import com.getmydrivercard.androidapp.models.PhoneNumberDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.validators.Patterns;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalDetailsFragment extends Fragment implements PersonalDetailsContracts.View {


    private PersonalDetailsContracts.Presenter mPresenter;

    @BindView(R.id.editText_personalDetails_egn)
    EditText egn;

    @BindView(R.id.editText_personalDetails_email)
    EditText email;

    @BindView(R.id.editText_personalDetails_firstName)
    EditText firstName;

    @BindView(R.id.editText_personalDetails_middleNames)
    EditText middleNames;

    @BindView(R.id.editText_personalDetails_surname)
    EditText surname;

    @BindView(R.id.button_personalDetails_savePersonalDetails)
    Button savePersonalDetails;

    @BindView(R.id.textView_personalDetails_dateOfBirth)
    TextView dateOfBirth;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private PersonalDetailsContracts.Navigator mNavigator;

    @Inject
    public PersonalDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_details, container, false);
        egn = view.findViewById(R.id.editText_personalDetails_egn);
        egn.setText("9605271111");
        egn.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    egn.setHint("");
                else
                    egn.setHint("Write You Egn Here");
            }
        });
        email = view.findViewById(R.id.editText_personalDetails_email);
        email.setText("bobi@bobi.com");
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    email.setHint("");
                else
                    email.setHint("Write You Email Here");
            }
        });
        firstName = view.findViewById(R.id.editText_personalDetails_firstName);
        firstName.setText("Borislav");
        firstName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    firstName.setHint("");
                else
                    firstName.setHint("Write You First Name Here");
            }
        });
        middleNames = view.findViewById(R.id.editText_personalDetails_middleNames);
        middleNames.setText("Dikran");
        middleNames.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    middleNames.setHint("");
                else
                    middleNames.setHint("Write You Middle Names Here");
            }
        });
        surname = view.findViewById(R.id.editText_personalDetails_surname);
        surname.setText("Bohosyan");
        surname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    surname.setHint("");
                else
                    surname.setHint("Write You Surname Here");
            }
        });
        dateOfBirth = view.findViewById(R.id.textView_personalDetails_dateOfBirth);
        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getContext(), android.R.style.Theme_Holo_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Log.d("date", "onDateSet: date: " + i + "/" + i1 + "/" + i2);
                dateOfBirth.setText(i + "/" + (i1 + 1) + "/" + i2);
            }
        };
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @OnClick(R.id.button_personalDetails_savePersonalDetails)
    public void onSuperheroSaveClicked() {
//        EditText egn;
//        EditText email;
//        EditText firstName;
//        EditText middleNames;
//        EditText surname;
//        TextView dateOfBirth;
        if (!Pattern.matches(Patterns.PATTERN_NUMBER, egn.getText().toString())) {
            egn.setError("EGN Must Contain Only Digits");
        }
        else if (!Pattern.matches(Patterns.PATTERN_EMAIL, email.getText().toString())) {
            email.setError("Email Is Not Valid");
        }
        else if (!Pattern.matches(Patterns.PATTERN_WORD, firstName.getText().toString())) {
            firstName.setError("First Name Must Contain Only Letters");
        }
        else if (!Pattern.matches(Patterns.PATTERN_WORD, middleNames.getText().toString())) {
            middleNames.setError("Middle Names Must Contain Only Letters");
        }
        else if (!Pattern.matches(Patterns.PATTERN_WORD, surname.getText().toString())) {
            surname.setError("Surname Must Contain Only Letters");
        }
        else if (dateOfBirth.getText().equals("")) {
            dateOfBirth.setError("Date Of Birth Should Be Picked");
        }else {
            if (getActivity().getIntent().getExtras().get("applicationType") == ApplicationType.NEW) {
                String item = getActivity().getIntent().getExtras().getString("token");
                OkHttpHttpRequester example = new OkHttpHttpRequester();
                int year = Integer.parseInt(dateOfBirth.getText().toString().split("/")[0]);
                int month = Integer.parseInt(dateOfBirth.getText().toString().split("/")[1]);
                int day = Integer.parseInt(dateOfBirth.getText().toString().split("/")[2]);
                Date date = new Date(year, month, day);
                PersonalDetailsDto personalDetailsDto = new PersonalDetailsDto(egn.getText().toString(), firstName.getText().toString(), middleNames.getText().toString(), surname.getText().toString(), date, email.getText().toString(), new AddressDto(EUCountry.BULGARIA, "2", "3", "4"), new PhoneNumberDto(EUCountry.BULGARIA, "123456789"));
                ResponseBody response = example.savePersonalDetails(personalDetailsDto, item);
                try {
                    Log.d("responsefragment", response.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto = new ApplyingForFirstDigitalTachographCardDto(ApplicationType.NEW, ApplicationStatus.PENDING, personalDetailsDto,
                        null, null, null, null, null);
                mNavigator.navigateToPersonalDetailsNew(applyingForFirstDigitalTachographCardDto);
            } else if (getActivity().getIntent().getExtras().get("applicationType") == ApplicationType.EXCHANGE) {
                String item = getActivity().getIntent().getExtras().getString("token");
                OkHttpHttpRequester example = new OkHttpHttpRequester();
                PersonalDetailsDto personalDetailsDto = new PersonalDetailsDto(egn.getText().toString(), firstName.getText().toString(), middleNames.getText().toString(), surname.getText().toString(), new Date(12345), email.getText().toString(), new AddressDto(EUCountry.BULGARIA, "2", "3", "4"), new PhoneNumberDto(EUCountry.BULGARIA, "123456789"));
                ResponseBody response = example.savePersonalDetails(personalDetailsDto, item);
                try {
                    Log.d("responsefragment", response.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto = new ExchangingEUCardForBGCardDto(ApplicationType.EXCHANGE, ApplicationStatus.PENDING, personalDetailsDto,
                        null, null, null, null, null, null, null, null, null);
                mNavigator.navigateToPersonalDetailsExchange(exchangingEUCardForBGCardDto);
            } else if (getActivity().getIntent().getExtras().get("applicationType") == ApplicationType.REPLACEMENT) {
                String item = getActivity().getIntent().getExtras().getString("token");
                OkHttpHttpRequester example = new OkHttpHttpRequester();
                PersonalDetailsDto personalDetailsDto = new PersonalDetailsDto(egn.getText().toString(), firstName.getText().toString(), middleNames.getText().toString(), surname.getText().toString(), new Date(12345), email.getText().toString(), new AddressDto(EUCountry.BULGARIA, "2", "3", "4"), new PhoneNumberDto(EUCountry.BULGARIA, "123456789"));
                ResponseBody response = example.savePersonalDetails(personalDetailsDto, item);
                try {
                    Log.d("responsefragment", response.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ReplacementOfCardDto replacementOfCardDto = new ReplacementOfCardDto(personalDetailsDto,
                        null, null, null, null, null, null, null, null, null, null);
                mNavigator.navigateToPersonalDetailsReplacement(replacementOfCardDto);
            } else if (getActivity().getIntent().getExtras().get("applicationType") == ApplicationType.RENEWAL) {
                String item = getActivity().getIntent().getExtras().getString("token");
                OkHttpHttpRequester example = new OkHttpHttpRequester();
                PersonalDetailsDto personalDetailsDto = new PersonalDetailsDto(egn.getText().toString(), firstName.getText().toString(), middleNames.getText().toString(), surname.getText().toString(), new Date(12345), email.getText().toString(), new AddressDto(EUCountry.BULGARIA, "2", "3", "4"), new PhoneNumberDto(EUCountry.BULGARIA, "123456789"));
                ResponseBody response = example.savePersonalDetails(personalDetailsDto, item);
                try {
                    Log.d("responsefragment", response.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                RenewalOfCardDto renewalOfCardDto = new RenewalOfCardDto(personalDetailsDto,
                        null, null, null, null, null, null, null, null, null, null);
                mNavigator.navigateToPersonalDetailsRenewal(renewalOfCardDto);
            }
        }
    }

    @Override
    public void setPresenter(PersonalDetailsContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(PersonalDetailsContracts.Navigator navigator) {
        mNavigator = navigator;
    }

}
