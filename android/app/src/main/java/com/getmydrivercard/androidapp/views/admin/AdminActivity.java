package com.getmydrivercard.androidapp.views.admin;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.PreviousActivity;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;
import com.getmydrivercard.androidapp.models.ApplicationListView;
import com.getmydrivercard.androidapp.models.ApplicationsDetailsDto;
import com.getmydrivercard.androidapp.validators.ApplicationAdapter;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.applicationdetails.ApplicationDetailsActivity;
import com.getmydrivercard.androidapp.views.menu.MenuActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;

public class AdminActivity extends BaseDrawerActivity implements AdminContracts.Navigator {
    public static final long IDENTIFIER = 333;
    ArrayAdapter<String> arrayAdapter;
    private List<String> textToVisualize;

    @Inject
    AdminFragment mView;

    @Inject
    AdminContracts.Presenter mPresenter;

    @BindView(R.id.listView_admin_allApplications)
    ListView lv;

    @BindView(R.id.editText_admin_filter)
    EditText filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        ButterKnife.bind(this);

        mView.setPresenter(mPresenter);
        mView.setNavigator(this);

        OkHttpHttpRequester example = new OkHttpHttpRequester();
        ResponseBody response = null;
        //GET TOKEN:
        ResponseBody response2 = example.getToken("admin", "123456");
        String token = null;
        String response3 = null;
        try {
            response3 = response2.string();
            token = response3.substring(17, response3.indexOf("\",\"token_type"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        response = example.getAllApplications(token);
        String responseString = null;
        try {
            responseString = response.string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectMapper mapper = new ObjectMapper();
        Log.d("responseString", responseString);
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<ApplicationsDetailsDto>>() {
        }.getType();
        ArrayList<ApplicationsDetailsDto> applicationsDetailsDtos = new Gson().fromJson(responseString, listType);
        textToVisualize = new ArrayList<>();
        for (int i = 0; i < applicationsDetailsDtos.size(); i++) {
            textToVisualize.add(applicationsDetailsDtos.get(i).getApplicationType() + " " + applicationsDetailsDtos.get(i).getApplicationStatus().toString());
        }
        lv = (ListView) findViewById(R.id.listView_admin_allApplications);
        arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                textToVisualize);

        lv.setAdapter(arrayAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getBaseContext(), ApplicationDetailsActivity.class);
                intent.putExtra("PreviousActivity", PreviousActivity.REGISTER);
                intent.putExtra("token", getIntent().getExtras().get("token").toString());
                intent.putExtra("aplicationDetails", applicationsDetailsDtos.get(i));
                startActivity(intent);
                finish();
            }
        });

        filter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                List<String> newList = new ArrayList<>();
                for (int j = 0; j < textToVisualize.size(); j++) {
                    if (textToVisualize.get(j).substring(0, charSequence.length()).toUpperCase().equals(charSequence.toString().toUpperCase())){
                        newList.add(textToVisualize.get(j));
                    }
                }
                arrayAdapter = new ArrayAdapter<String>(
                        getBaseContext(),
                        android.R.layout.simple_list_item_1,
                        newList);
                lv.setAdapter(arrayAdapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }
}