package com.getmydrivercard.androidapp.views.menu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.enums.PreviousActivity;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.exchange.ExchangeActivity;
import com.getmydrivercard.androidapp.views.personaldetails.PersonalDetailsActivity;
import com.getmydrivercard.androidapp.views.replacement.ReplacementActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class MenuActivity extends BaseDrawerActivity implements MenuContracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    MenuFragment mView;

    @Inject
    MenuContracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);

        mView.setPresenter(mPresenter);
        mView.setNavigator(this);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToPersonalDetailsNew() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("applicationType", ApplicationType.NEW);
        intent.putExtra("PreviousActivity", PreviousActivity.NEW);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsExchange() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("applicationType", ApplicationType.EXCHANGE);
        intent.putExtra("PreviousActivity", PreviousActivity.EXCHANGE);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsRenewal() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("applicationType", ApplicationType.RENEWAL);
        intent.putExtra("PreviousActivity", PreviousActivity.RENEWAL);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsReplacement1() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
        intent.putExtra("PreviousActivity", PreviousActivity.REPLACEMENT1);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToPersonalDetailsReplacement2() {
        Intent intent = new Intent(this, PersonalDetailsActivity.class);
        intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
        intent.putExtra("PreviousActivity", PreviousActivity.REPLACEMENT2);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        startActivity(intent);
        finish();
    }
}
