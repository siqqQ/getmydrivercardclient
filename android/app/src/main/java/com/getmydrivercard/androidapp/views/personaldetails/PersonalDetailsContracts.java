package com.getmydrivercard.androidapp.views.personaldetails;

import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;

public interface PersonalDetailsContracts {
    interface View {

        void setPresenter(PersonalDetailsContracts.Presenter presenter);

        void showError(Throwable throwable);

        void hideLoading();

        void showLoading();
    }

    interface Presenter {

        void subscribe(PersonalDetailsContracts.View view);

        void unsubscribe();
    }

    public interface Navigator {

        void navigateToPersonalDetailsNew(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto);
        void navigateToPersonalDetailsExchange(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto);
        void navigateToPersonalDetailsReplacement(ReplacementOfCardDto replacementOfCardDto);
        void navigateToPersonalDetailsRenewal(RenewalOfCardDto renewalOfCardDto);
    }
}
