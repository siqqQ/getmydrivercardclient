package com.getmydrivercard.androidapp.views.personaldetails2;

import android.content.Intent;
import android.os.Bundle;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.enums.ApplicationType;
import com.getmydrivercard.androidapp.models.ApplyingForFirstDigitalTachographCardDto;
import com.getmydrivercard.androidapp.models.ExchangingEUCardForBGCardDto;
import com.getmydrivercard.androidapp.models.RenewalOfCardDto;
import com.getmydrivercard.androidapp.models.ReplacementOfCardDto;
import com.getmydrivercard.androidapp.views.BaseDrawerActivity;
import com.getmydrivercard.androidapp.views.exchange.ExchangeActivity;
import com.getmydrivercard.androidapp.views.newtahographcard.NewTahographCardActivity;
import com.getmydrivercard.androidapp.views.renewal.RenewalActivity;
import com.getmydrivercard.androidapp.views.replacement.ReplacementActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class PersonalDetails2Activity extends BaseDrawerActivity implements PersonalDetails2Contracts.Navigator {
    public static final long IDENTIFIER = 290;

    @Inject
    PersonalDetails2Fragment mView;

    @Inject
    PersonalDetails2Contracts.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details2);
        ButterKnife.bind(this);
        mView.setPresenter(mPresenter);
        mView.setNavigator(this);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mView)
                .commit();
    }

    @Override
    protected long getIdentifier() {
        return IDENTIFIER;
    }

    @Override
    public void navigateToNew(ApplyingForFirstDigitalTachographCardDto applyingForFirstDigitalTachographCardDto) {
            Intent intent = new Intent(this, NewTahographCardActivity.class);
            intent.putExtra("applicationType", ApplicationType.NEW);
            intent.putExtra("token", getIntent().getExtras().get("token").toString());
            intent.putExtra("application", applyingForFirstDigitalTachographCardDto);
            intent.putExtra("personalDetails", getIntent().getExtras().getSerializable("personalDetails"));
            startActivity(intent);
            finish();
    }

    @Override
    public void navigateToExchange(ExchangingEUCardForBGCardDto exchangingEUCardForBGCardDto) {
        Intent intent = new Intent(this, ExchangeActivity.class);
        intent.putExtra("applicationType", ApplicationType.EXCHANGE);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("application", exchangingEUCardForBGCardDto);
        intent.putExtra("personalDetails", getIntent().getExtras().getSerializable("personalDetails"));
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToReplacement(ReplacementOfCardDto replacementOfCardDto) {
        Intent intent = new Intent(this, ReplacementActivity.class);
        intent.putExtra("applicationType", ApplicationType.REPLACEMENT);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("application", replacementOfCardDto);
        intent.putExtra("personalDetails", getIntent().getExtras().getSerializable("personalDetails"));
        startActivity(intent);
        finish();
    }

    @Override
    public void navigateToRenewal(RenewalOfCardDto renewalOfCardDto) {
        Intent intent = new Intent(this, RenewalActivity.class);
        intent.putExtra("applicationType", ApplicationType.RENEWAL);
        intent.putExtra("token", getIntent().getExtras().get("token").toString());
        intent.putExtra("application", renewalOfCardDto);
        intent.putExtra("personalDetails", getIntent().getExtras().getSerializable("personalDetails"));
        startActivity(intent);
        finish();
    }
}
