package com.getmydrivercard.androidapp.diconfig;

import com.getmydrivercard.androidapp.views.register.RegisterContracts;
import com.getmydrivercard.androidapp.views.register.RegisterFragment;
import com.getmydrivercard.androidapp.views.register.RegisterPresenter;
import com.getmydrivercard.androidapp.views.replacement.ReplacementContracts;
import com.getmydrivercard.androidapp.views.replacement.ReplacementFragment;
import com.getmydrivercard.androidapp.views.replacement.ReplacementPresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ReplacementModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract ReplacementFragment replacementFragment();

    @ActivityScoped
    @Binds
    abstract ReplacementContracts.Presenter replacementPresenter(ReplacementPresenter presenter);
}
