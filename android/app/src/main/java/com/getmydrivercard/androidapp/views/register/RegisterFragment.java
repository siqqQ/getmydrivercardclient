package com.getmydrivercard.androidapp.views.register;


import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.getmydrivercard.androidapp.R;
import com.getmydrivercard.androidapp.http.OkHttpHttpRequester;
import com.getmydrivercard.androidapp.models.DrivingLicenseCategory;
import com.getmydrivercard.androidapp.models.DrivingLicenseDto;
import com.getmydrivercard.androidapp.models.EUCountry;
import com.getmydrivercard.androidapp.models.UserDto;

import java.io.IOException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment implements RegisterContracts.View {


    private RegisterContracts.Presenter mPresenter;

    @BindView(R.id.button_register_registerUser)
    Button registerUserButton;

    @BindView(R.id.editText_register_email)
    EditText email;

    @BindView(R.id.editText_register_issuingAuthority)
    EditText issuingAuthority;

    @BindView(R.id.editText_register_password)
    EditText password;

    @BindView(R.id.editText_register_username)
    EditText username;

    @BindView(R.id.spinner_register_drivingLicenseCategories)
    Spinner drivingLicenseCategory;

    @BindView(R.id.spinner_register_issuingCountry)
    Spinner issuingCountry;

    private RegisterContracts.Navigator mNavigator;

    @Inject
    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @OnClick(R.id.button_register_registerUser)
    public void onSuperheroSaveClicked() {
        OkHttpHttpRequester example = new OkHttpHttpRequester();
        ResponseBody response = null;
        try {
            response = example.createNewUser(new UserDto(username.getText().toString(), password.getText().toString(), email.getText().toString(),
                    new DrivingLicenseDto(EUCountry.valueOf(issuingCountry.getSelectedItem().toString().toUpperCase()), DrivingLicenseCategory.valueOf(drivingLicenseCategory.getSelectedItem().toString()), issuingAuthority.getText().toString())));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ResponseBody response2 = example.getToken(username.getText().toString(), password.getText().toString());
        String token = null;
        String response3 = null;
        try {
            response3 = response2.string();
            token = response3.substring(17, response3.indexOf("\",\"token_type"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mNavigator.navigateToPersonalDetails(token);
    }

    @Override
    public void setPresenter(RegisterContracts.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void navigateToPersonalDetails(String response3) {
        mNavigator.navigateToPersonalDetails(response3);
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading() {

    }

    public void setNavigator(RegisterContracts.Navigator navigator) {
        mNavigator = navigator;
    }

}
